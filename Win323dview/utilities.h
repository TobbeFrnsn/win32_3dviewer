#ifndef _UTILITIES_H_
#define _UTILITIES_H_

using namespace std;

#define _USE_MATH_DEFINES

//#include "GL/glew.h"



#include <math.h>
#include <stdio.h>
#include <stdlib.h>			//C standard lib
#include <string.h>			//C string lib
#include <time.h>
#include <fstream>

#include "algebra.h"
#include "mesh.h"
#include "Vec3.h"



//Just put neccecary code in here to not clutter the other files 

enum rtype{
	wireframe = 0,
	flat,
	gouraud,
	phong,
	toon,
	rtFirst = wireframe,
	rtLast = toon,
	basicFirst = flat,
	basicLast = gouraud
};

void PrintRenderType(rtype r);
string ReadTextFile(const char * filename);
//GLuint initShader(const char * vert, const char * frag);
void ClearScreen();
//void ExtractFrustum(float frustum[6][4]);
//bool SphereInFrustum( float x, float y, float z, float radius,float frustum[6][4]);
//void CalculateBoundingSphere(Mesh * mesh,int stride);
//void calculateFlatNormals(Mesh *mesh);
//void calculateGouraudNormals(Mesh *mesh);
//void renderGouraudShading(Mesh *mesh);
//void renderFlatShading(Mesh *mesh);
//void renderWireframe(Mesh *mesh);
Vec3f Reflect(Vec3f N,Vec3f V);
void RotateX(Vec3f & vec,float q);
void RotateY(Vec3f & vec,float q);
void RotateZ(Vec3f & vec,float q);

double diffclock(clock_t clock1,clock_t clock2);

float toRadians(float val);

void glSetPixel(int x, int y, Vec3f & c);

void moveX(Vec3f * pos,Vec3f * rot,float v);

void moveY(Vec3f * pos,Vec3f * rot,float v);

void moveZ(Vec3f * pos,Vec3f * rot,float v);

void ExtractFrustum(double frustum[6][4]);

bool SphereInFrustum( double x, double y, double z, double radius,double frustum[6][4] );

void CalculateBoundingSphere(Mesh * mesh,int stride);

void calculateFlatNormals(Mesh * mesh);

void calculateGouraudNormals(Mesh *mesh);

void renderGouraudShading(Mesh *mesh);

void renderFlatShading(Mesh * mesh);

void renderWireframe(Mesh * mesh);

#endif 