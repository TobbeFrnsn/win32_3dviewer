#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <math.h>

//#include "GL/glew.h"
//#include "GL/glut.h"

#include "Vec3.h"
#include "Ray.h"
#include "Light.h"



using namespace std;

//class GFXObject {
//public:
//
//	Vec3f c;
//	float r;
//	Vec3f color;
//	float reflection;
//	float ambient;
//	float specular;
//	float diffuse;
//	
//public:
//
//	Sphere(const Vec3f & cen, float rad,const Vec3f & col,float refl) : c(cen), r(rad), color(col), reflection(refl) { }
//	void setupMaterial(const float a,const float d,const float s) {ambient = a; specular=s; diffuse=d; }
//	float GetAmbient(){return ambient;}
//	float GetSpecular(){return specular;}
//	float GetDiffuse(){return diffuse;}
//	Vec3f GetPosition(){return c;}
//	bool hit(const Ray & r, HitRec & rec, int & numberoftests) const;
//	void computeSurfaceHitFields(const Ray & r, HitRec & rec) const;
//	void draw(){}
//
//};

class Sphere /*: public GFXObject*/ {
public:

	Vec3f c;
	float r;
	Vec3f color;
	float reflection;
	float ambient;
	float specular;
	float diffuse;
	
public:

	Sphere(const Vec3f & cen, float rad,const Vec3f & col,float refl) : c(cen), r(rad), color(col), reflection(refl) { }
	void setupMaterial(const float a,const float d,const float s) {ambient = a; specular=s; diffuse=d; }
	float GetAmbient(){return ambient;}
	float GetSpecular(){return specular;}
	float GetDiffuse(){return diffuse;}
	Vec3f & GetPosition(){return c;}
	bool hit(const Ray & r, HitRec & rec, long & numberoftests) const;
	void computeSurfaceHitFields(const Ray & r, HitRec & rec) const;
	void draw(){}

};




#endif