#ifndef _MESH_H_
#define _MESH_H_

#include "algebra.h"

typedef struct _Triangle {
	int vInds[3]; //vertex indices
	Vector norm;
} Triangle;

typedef struct _Sphere {
	Vector center;
	double radius;
}BoundSphere;

typedef struct _Mesh { 
	int nv;				
	Vector *vertices;	
	Vector *vertexNormals;
	int nt;				
	Triangle *triangles;
	struct _Mesh *next; 
	Vector scaling;
	Vector rotation;
	Vector translation; 
	BoundSphere boundingSphere;
} Mesh;

//typedef struct _Camera {
//	Vector position;
//	Vector rotation;
//	double fov; 
//	double nearPlane; 
//	double farPlane; 
//} Camera;

void insertModel(Mesh **list, int nv, float * vArr, int nt, int * tArr, double scale, double posX, double posY, double posZ, double rotX, double rotY, double rotZ,double scaleXYZ);

#endif
