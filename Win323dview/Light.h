#ifndef _LIGHT_H_
#define _LIGHT_H_


class Light {
public:
	Vec3f position;
	Vec3f ambient;
	Vec3f specular;
	Vec3f diffuse;
public:

	Light(){}
	Light(const Vec3f & p,const Vec3f & a,const Vec3f & s,const Vec3f & d) : position(p), ambient(a), specular(s), diffuse(d) { }

	Vec3f & GetPosition(){return position;}
	
	void SetPosition(Vec3f p){position = p;}

	Vec3f & GetAmbient(){return ambient;}
	Vec3f & GetSpecular(){return specular;}
	Vec3f & GetDiffuse(){return diffuse;}

};

#endif