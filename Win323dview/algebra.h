#ifndef _ALGEBRA_H_
#define _ALGEBRA_H_

//#include "mesh.h"

typedef struct { double x, y, z; } Vector;
typedef struct { double x, y, z, w; } HomVector;

/* Column-major order are used for the matrices here to be comnpatible with OpenGL.
** The indices used to access elements in the matrices are shown below.
**  _                _
** |                  |
** |   0   4   8  12  |
** |                  |
** |   1   5   9  13  |
** |                  |
** |   2   6  10  14  |
** |                  |
** |   3   7  11  15  |
** |_                _|
*/
typedef struct matrix { double e[16]; } Matrix;

Vector Add(Vector a, Vector b);
Vector Subtract(Vector a, Vector b);
Vector CrossProduct(Vector a, Vector b);
double DotProduct(Vector a, Vector b);
double Length(Vector a);
Vector Normalize(Vector a);
Vector ScalarVecMul(double t, Vector a);
HomVector MatVecMul(Matrix a, Vector b);
Vector Homogenize(HomVector a);
Matrix MatMatMul(Matrix a, Matrix b);
void PrintMatrix(char *name, Matrix m);
void PrintVector(char *name, Vector v);
void PrintHomVector(char *name, HomVector h);
Matrix RotationX(double angle);
Matrix RotationY(double angle);
Matrix RotationZ(double angle);
Matrix Perspective(double fovy,double aspect,double near,double far);
Matrix Translation(double x,double y,double z);
Matrix IdentityMatrix();
Matrix Ortho(double  left,  double  right,  double  bottom,  double  top,  double  nearVal,  double  farVal);
Matrix Scale(double x,double y,double z);
Vector ComputeTriangleNormal(Vector v0,Vector v1,Vector v2);
Vector ZeroVector();
Vector Div(Vector a, Vector b);
double SquareLength(Vector a);


#endif

