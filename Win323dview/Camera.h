#ifndef _CAMERA_H_
#define _CAMERA_H_



#include "Vec3.h"
#include "Ray.h"



class Camera {
private:
	Vec3f position;

	Vec3f lookat;
	Vec3f yaxis;
	Vec3f xaxis;

	Vec3f rotation;
	float angle;
	float zFar;
	float zNear;
	float fovH;
	float fovW;


public:
	Camera(Vec3f pos,Vec3f rot,float a,float zF,float zN,float fW,float fH,Vec3f lA,Vec3f u,Vec3f le){
		position = pos;
		rotation = rot;
		angle = a;
		zFar = zF;
		zNear = zN; 
		fovH = fH;
		fovW = fW; 
		lookat = lA;
		yaxis = u;
		xaxis = le;


	}
	Camera() {position = Vec3f(0.0f,0.0f,0.0f);rotation = Vec3f(0.0f,0.0f,0.0f); }
	Vec3f * Pos(){return &position;}
	Vec3f * Rot(){return &rotation;}
	Vec3f & LookAt(){return lookat;}
	Vec3f & YAxis(){return yaxis;}
	Vec3f & XAxis(){return xaxis;}


	void Print();
};

#endif