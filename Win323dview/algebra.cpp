#define _USE_MATH_DEFINES // To get M_PI defined
#include <math.h>
#include <stdio.h>
#include "algebra.h"


//http://www.crownandcutlass.com/features/technicaldetails/frustum.html


double cotan(double i) { return(1 / tan(i)); }

Vector CrossProduct(Vector a, Vector b) {
	Vector v = { a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x };
	return v;
}

double DotProduct(Vector a, Vector b) {
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

Vector Subtract(Vector a, Vector b) {
	Vector v = { a.x-b.x, a.y-b.y, a.z-b.z };
	return v;
}    

Vector Add(Vector a, Vector b) {
	Vector v = { a.x+b.x, a.y+b.y, a.z+b.z };
	return v;
}    

Vector Div(Vector a, Vector b) {
	Vector v = { a.x/b.x, a.y/b.y, a.z/b.z };
	return v;
}  

double Length(Vector a) {
	return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}

Vector Normalize(Vector a) {
	double len = Length(a);
	Vector v = { a.x/len, a.y/len, a.z/len };
	return v;
}

double SquareLength(Vector a) {
	double len = Length(a);
	return len * len;
}

Vector ScalarVecMul(double t, Vector a) {
	Vector b = { t*a.x, t*a.y, t*a.z };
	return b;
}



HomVector MatVecMul(Matrix a, Vector b) {
	HomVector h;
	h.x = b.x*a.e[0] + b.y*a.e[4] + b.z*a.e[8] + a.e[12];
	h.y = b.x*a.e[1] + b.y*a.e[5] + b.z*a.e[9] + a.e[13];
	h.z = b.x*a.e[2] + b.y*a.e[6] + b.z*a.e[10] + a.e[14];
	h.w = b.x*a.e[3] + b.y*a.e[7] + b.z*a.e[11] + a.e[15];
	return h;
}

Vector Homogenize(HomVector h) {
	Vector a;
	if (h.w == 0.0) {
		fprintf(stderr, "Homogenize: w = 0\n");
		a.x = a.y = a.z = 9999999;
		return a;
	}
	a.x = h.x / h.w;
	a.y = h.y / h.w;
	a.z = h.z / h.w;
	return a;
}

Matrix MatMatMul(Matrix a, Matrix b) {
	Matrix c;
	int i, j, k;
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			c.e[j*4+i] = 0.0;
			for (k = 0; k < 4; k++)
				c.e[j*4+i] += a.e[k*4+i] * b.e[j*4+k];
		}
	}
	return c;
}

void PrintVector(char *name, Vector a) {
	printf("%s: %6.5lf %6.5lf %6.5lf\n", name, a.x, a.y, a.z);
}

void PrintHomVector(char *name, HomVector a) {
	printf("%s: %6.5lf %6.5lf %6.5lf %6.5lf\n", name, a.x, a.y, a.z, a.w);
}

void PrintMatrix(char *name, Matrix a) { 
	int i,j;

	printf("%s:\n", name);
	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			printf("%6.5lf ", a.e[j*4+i]);
		}
		printf("\n");
	}
}

Matrix RotationX(double x)
{
	Matrix m;

	x = x * M_PI / 180;

	m.e[0] = 1;
	m.e[4] = 0;
	m.e[8] = 0;
	m.e[12] = 0;

	m.e[1] = 0;
	m.e[5] = cos(x);
	m.e[9] = -sin(x);
	m.e[13] = 0;

	m.e[2] = 0;
	m.e[6] = sin(x);
	m.e[10] = cos(x);
	m.e[14] = 0;

	m.e[3] = 0;
	m.e[7] = 0;
	m.e[11] = 0;
	m.e[15] = 1;

	return m;
}

Matrix RotationY(double x)
{
	Matrix m;

	x = x * M_PI / 180;
	
	m.e[0] = cos(x);
	m.e[4] = 0;
	m.e[8] = sin(x);
	m.e[12] = 0;

	m.e[1] = 0;
	m.e[5] = 1;
	m.e[9] = 0;
	m.e[13] = 0;

	m.e[2] = -sin(x);
	m.e[6] = 0;
	m.e[10] = cos(x);
	m.e[14] = 0;

	m.e[3] = 0;
	m.e[7] = 0;
	m.e[11] = 0;
	m.e[15] = 1;
	
	return m;
	
}

Matrix RotationZ(double x)
{
	Matrix m;

	x = x * M_PI / 180;
	
	m.e[0] = cos(x);
	m.e[4] = -sin(x);
	m.e[8] = 0;
	m.e[12] = 0;

	m.e[1] = sin(x);
	m.e[5] = cos(x);
	m.e[9] = 0;
	m.e[13] = 0;

	m.e[2] = 0;
	m.e[6] = 0;
	m.e[10] = 1;
	m.e[14] = 0;

	m.e[3] = 0;
	m.e[7] = 0;
	m.e[11] = 0;
	m.e[15] = 1;
	
	return m;
}

Matrix Perspective(double fovy,double aspect,double near,double far)
{
		Matrix m;
		double f = cotan((fovy * M_PI / 180) / 2);

		//1
		m.e[0] = f/aspect;
		m.e[4] = 0;
		m.e[8] = 0;
		m.e[12] = 0;
		//2
		m.e[1] = 0;
		m.e[5] = f;
		m.e[9] = 0;
		m.e[13] = 0;
		//3
		m.e[2] = 0;	
		m.e[6] = 0;
		m.e[10] = (far + near) / (near - far);
		m.e[14] = (2 * far * near) / (near - far);
		//4
		m.e[3] = 0;
		m.e[7] = 0;
		m.e[11] = -1;
		m.e[15] = 0;

		return m;
}

Matrix Ortho(double  left,  double  right,  double  bottom,  double  top,  double  nearVal,  double  farVal)
{
		Matrix m;
		double tx = -(right + left) /(right - left);
		double ty = -(top + bottom)/(top - bottom);
		double tz = -(farVal + nearVal)/(farVal - nearVal);

		//1
		m.e[0] = 2 / (right - left);
		m.e[4] = 0;
		m.e[8] = 0;
		m.e[12] = tx;
		//2
		m.e[1] = 0;
		m.e[5] = 2 / (top - bottom);
		m.e[9] = 0;
		m.e[13] = ty;
		//3
		m.e[2] = 0;	
		m.e[6] = 0;
		m.e[10] = -2 / (farVal - nearVal);
		m.e[14] = tz;
		//4
		m.e[3] = 0;
		m.e[7] = 0;
		m.e[11] = 0;
		m.e[15] = 1;

		return m;
}

Matrix Translation(double x,double y,double z)
{
		Matrix m;

		//Translation
		m.e[0] = 1;
		m.e[4] = 0;
		m.e[8] = 0;
		m.e[12] = x;

		m.e[1] = 0;
		m.e[5] = 1;
		m.e[9] = 0;
		m.e[13] = y;

		m.e[2] = 0;	
		m.e[6] = 0;
		m.e[10] = 1;
		m.e[14] = z;

		m.e[3] = 0;
		m.e[7] = 0;
		m.e[11] = 0;
		m.e[15] = 1;


		return m;
}

Matrix Scale(double x,double y,double z)
{
	Matrix m;
	
	m.e[0] = x;
	m.e[4] = 0;
	m.e[8] = 0;
	m.e[12] = 0;

	m.e[1] = 0;
	m.e[5] = y;
	m.e[9] = 0;
	m.e[13] = 0;

	m.e[2] = 0;
	m.e[6] = 0;
	m.e[10] = z;
	m.e[14] = 0;

	m.e[4] = 0;
	m.e[7] = 0;
	m.e[11] = 0;
	m.e[15] = 1;
	
	return m;
}

Matrix IdentityMatrix()
{
	Matrix m;
	
	m.e[0] = 1;
	m.e[4] = 0;
	m.e[8] = 0;
	m.e[12] = 0;

	m.e[1] = 0;
	m.e[5] = 1;
	m.e[9] = 0;
	m.e[13] = 0;

	m.e[2] = 0;
	m.e[6] = 0;
	m.e[10] = 1;
	m.e[14] = 0;

	m.e[4] = 0;
	m.e[7] = 0;
	m.e[11] = 0;
	m.e[15] = 1;
	
	return m;
}

Vector ComputeTriangleNormal(Vector v0,Vector v1,Vector v2)
{
	return Normalize(CrossProduct(Subtract(v1,v0),Subtract(v2,v0)));
}

Vector ZeroVector() {
	Vector b = { 0, 0, 0 };
	return b;
}
