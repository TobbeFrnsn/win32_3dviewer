//#include "SSE.h"
//
//
//void fireRaysSSE(void) { 
//
//		const float coef = 0.25f; 
//
//		Ray ray;
//		HitRec hitRec;Vec3f final_color;
//
//		ray.o = Vec3f(scene->cam.Pos()->x, scene->cam.Pos()->y, scene->cam.Pos()->z);
//
//		if(useAA)
//
//		for (int y = 0; y < image->getHeight(); y++) {
//			for (int x = 0; x < image->getWidth(); x++) {
//
//				final_color = Vec3f(0.0f, 0.0f, 0.0f);  
//
//				//4 x Supersampling loop
//				for(float fx = x; fx < x + 1.0f; fx += 0.5f){
//					for(float fy = y; fy < y + 1.0f; fy += 0.5f)
//					{ 
//						ray.d = getEyeRayDirection(fx, fy);
//
//						hitRec.anyHit = false;
//
//						if(searchClosestHit(ray, hitRec))//Find closest hitpoint
//						{
//							//Get the resulting color for that pixel
//							final_color += GetPhongReflectionAndShadows(ray,hitRec,x,y) * coef;
//						}
//					}
//				}
//				//Finally set the color
//				image->setPixel(x, y, final_color);
//				glSetPixel(x, y, final_color);
//			}
//		}
//
//		else
//
//			for (int y = 0; y < image->getHeight(); y++) {
//			for (int x = 0; x < image->getWidth(); x++) {
//
//				final_color = Vec3f(0.0f, 0.0f, 0.0f);  
//
//			
//						ray.d = getEyeRayDirection(x, y);
//
//						hitRec.anyHit = false;
//
//						if(searchClosestHit(ray, hitRec))
//						{
//							final_color += GetPhongReflectionAndShadows(ray,hitRec,x,y);
//						}
//
//
//					image->setPixel(x, y, final_color);
//				glSetPixel(x, y, final_color);
//				}
//
//				
//				
//				
//			}
//		}
//
//Vec3f GetPhongReflectionAndShadowsSSE(Ray & ray,HitRec & hitRec,float x,float y)
//	{
//		Vec3f final_color = Vec3f(0.0f,0.0f,0.0f);
//
//		Ray shadowRay;
//		HitRec shadowRec;
//
//		Ray refRay;
//		HitRec refRec;
//
//		Vec3f computed_color = Vec3f(scene->spheres[hitRec.sphereHit].color.r,scene->spheres[hitRec.sphereHit].color.g,scene->spheres[hitRec.sphereHit].color.b) * scene->spheres[hitRec.sphereHit].GetAmbient();
//
//		for(int i = 0;i < NUM_OF_LIGHTS;i++)
//		{
//			Vec3f intesity = Vec3f(0.0f,0.0f,0.0f);
//			shadowRay.d = (scene->lights[i].GetPosition() - hitRec.p).normalize();
//			shadowRay.o = hitRec.p;
//			shadowRec.anyHit = false;
//			Vec3f shRay = shadowRay.d;
//			Vec3f N = hitRec.n;
//			Vec3f L = shRay;
//			Vec3f V = - shRay;
//			Vec3f R = Reflect(N,V);//N * 2 * (V.dot(N)) - V;
//			Vec3f VV = - ray.d;
//			Vec3f RR = Reflect(N,VV);//N * 2 * (VV.dot(N)) - VV;
//			refRec.anyHit = false;
//			refRec.col = Vec3f(0.0f,0.0f,0.0f);
//			refRec.sphereHit = hitRec.sphereHit;
//			refRay.d = RR;
//			refRay.o = hitRec.p;
//			refRec.primIndex = recDepth;
//			float lambertTerm = N.dot(L);
//
//			SearchReflectionHit(refRay,refRec);
//
//			if(lambertTerm > 0.0f)
//			{
//				searchHitToLight(shadowRay,shadowRec,(scene->lights[i].GetPosition() - hitRec.p).len(),hitRec.sphereHit);
//				intesity += scene->lights[i].GetDiffuse() * scene->spheres[hitRec.sphereHit].GetDiffuse() * lambertTerm;
//				//float specular = pow( max(R.dot(L), 0.0f),100);
//				float specular = pow( max(R.dot(ray.d), 0.0f),100);
//				intesity += scene->lights[i].GetSpecular() * scene->spheres[hitRec.sphereHit].GetSpecular() * specular;
//
//			}else
//			{
//				shadowRec.anyHit = true;
//			}
//
//			computed_color += scene->lights[i].GetAmbient();
//
//			if (refRec.anyHit) {
//				computed_color += refRec.col;
//			}
//
//
//			if (shadowRec.anyHit) {
//				computed_color *= 0.25;
//			}else
//			{
//				computed_color += intesity; 
//			}
//
//			final_color += computed_color;
//
//			computed_color = Vec3f(0.0f,0.0f,0.0f);
//
//		}
//
//		return final_color;
//
//	}
//
//
//void searchClosestHitReflectionSSE(const Ray & ray, HitRec & hitRec,int sp) {
//		float len = 90000000000000000.0f;
//		int ilen = scene->spheres.size(); 
//		hitRec.anyHit = false;
//		hitRec.tHit = 0.0f;
//		HitRec temp;
//		temp = hitRec;
//
//		for (int i = 0; i < ilen; ++i) {
//
//			if(scene->spheres[i].hit(ray, temp, numberOfTests) && i != sp)
//			{
//				if(temp.tHit < len){
//					scene->spheres[i].computeSurfaceHitFields(ray,temp);
//					temp.anyHit = true;
//					temp.sphereHit = i;
//					len = temp.tHit;
//					hitRec = temp;
//				}
//			}
//		}
//	}
//
//void SearchReflectionHitSSE(const Ray & ray, HitRec & hitRec) {
//
//		hitRec.col = Vec3f(0.0f,0.0f,0.0f);
//
//		if(scene->spheres[hitRec.sphereHit].reflection < 0 || hitRec.primIndex <= 0)return;
//
//		searchClosestHitReflection(ray,hitRec,hitRec.sphereHit);
//
//		if(hitRec.anyHit == true)
//		{
//			HitRec refRec;
//			Ray refRay;
//
//			Vec3f N = hitRec.n;
//			Vec3f V = - ray.d;V.normalize();
//			Vec3f R = Reflect(N,V);//N * 2 * V.dot(N) - V;
//			R.normalize();
//
//			refRay.o = hitRec.p;
//			refRay.d = R;
//
//			hitRec.col = scene->spheres[hitRec.sphereHit].color * scene->spheres[hitRec.sphereHit].reflection;
//
//			refRec.anyHit = false;
//			refRec.primIndex = hitRec.primIndex - 1;
//			refRec.sphereHit = hitRec.sphereHit;
//
//			SearchReflectionHit(refRay,refRec);
//
//			hitRec.col += refRec.col;
//
//		}
//	}
//
////Serach the closest point in for the ray
//bool searchClosestHitSSE(const Ray & ray, HitRec & hitRec) {
//		float len = 90000000000000000.0f;
//		int ilen = scene->spheres.size(); 
//		hitRec.anyHit = false;
//		hitRec.tHit = 0.0f;
//		HitRec temp;
//		temp = hitRec;
//
//		for (int i = 0; i < ilen; ++i) {
//
//			if(scene->spheres[i].hit(ray, temp, numberOfTests))
//			{
//				if(temp.tHit < len){
//					scene->spheres[i].computeSurfaceHitFields(ray,temp);
//					temp.anyHit = true;
//					temp.sphereHit = i;
//					len = temp.tHit;
//					hitRec = temp;
//				}
//			}
//		}
//
//		return hitRec.anyHit;
//	}
////Research if a point is illuminated or not	
//void searchHitToLightSSE(const Ray & ray, HitRec & hitRec,float dist,int sp) {
//		
//		float len = 90000000000000000.0f;
//		int ilen = scene->spheres.size();
//		hitRec.anyHit = false;
//		hitRec.tHit = 0.0f;
//
//		HitRec temp;
//		temp = hitRec;
//
//		for (int i = 0; i < ilen; ++i) {
//
//			if(scene->spheres[i].hit(ray, temp, numberOfTests) && i != sp)
//			{
//				if(temp.tHit < len){
//					scene->spheres[i].computeSurfaceHitFields(ray,temp);
//					temp.anyHit = true;
//					temp.sphereHit = i;
//					len = temp.tHit;
//					hitRec = temp;
//				}
//			}
//		}
//
//		if(hitRec.anyHit)
//		{
//			if(hitRec.tHit > dist)
//					hitRec.anyHit = false;
//
//		}
//
//	}