#include "bitmap.h"

using namespace std;

/*
CreateBitmapInfoStruct function used from MSDN

Reference: xhttp://msdn.microsoft.com/en-us/library/windows/desktop/dd145119(v=vs.85).aspx

Small Changes made to error handling

*/

PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp)
{ 
    BITMAP bmp; 
    PBITMAPINFO pbmi; 
    WORD    cClrBits; 

    // Retrieve the bitmap color format, width, and height.  
    if (!GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp)) 
       {cout<<"Error "<<"GetObject"<<endl;return false;}//CloseHandleerrhandler("GetObject", hwnd); 

    // Convert the color format to a count of bits.  
    cClrBits = 24;//(WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
    if (cClrBits == 1) 
        cClrBits = 1; 
    else if (cClrBits <= 4) 
        cClrBits = 4; 
    else if (cClrBits <= 8) 
        cClrBits = 8; 
    else if (cClrBits <= 16) 
        cClrBits = 16; 
    else if (cClrBits <= 24) 
        cClrBits = 24; 
    else cClrBits = 32; 

    // Allocate memory for the BITMAPINFO structure. (This structure  
    // contains a BITMAPINFOHEADER structure and an array of RGBQUAD  
    // data structures.)  

     if (cClrBits < 24) 
         pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
                    sizeof(BITMAPINFOHEADER) + 
                    sizeof(RGBQUAD) * (1<< cClrBits)); 

     // There is no RGBQUAD array for these formats: 24-bit-per-pixel or 32-bit-per-pixel 

     else 
         pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
                    sizeof(BITMAPINFOHEADER)); 

    // Initialize the fields in the BITMAPINFO structure.  
    pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
    pbmi->bmiHeader.biWidth = bmp.bmWidth; 
    pbmi->bmiHeader.biHeight = bmp.bmHeight; 
    pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
    pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
    if (cClrBits < 24) 
        pbmi->bmiHeader.biClrUsed = (1<<cClrBits); 

    // If the bitmap is not compressed, set the BI_RGB flag.  
    pbmi->bmiHeader.biCompression = BI_RGB; 

    // Compute the number of bytes in the array of color  
    // indices and store the result in biSizeImage.  
    // The width must be DWORD aligned unless the bitmap is RLE 
    // compressed. 
    pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits +31) & ~31) /8
                                  * pbmi->bmiHeader.biHeight; 
    // Set biClrImportant to 0, indicating that all of the  
    // device colors are important.  
     pbmi->bmiHeader.biClrImportant = 0; 
     return pbmi; 
 } 


/*
CreateBMPFile function used from MSDN

Reference: xhttp://msdn.microsoft.com/en-us/library/windows/desktop/dd145119(v=vs.85).aspx

Small Changes made to error handling

*/
bool CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, 
                  HBITMAP hBMP, HDC hDC) 
 { 
     HANDLE hf;                 // file handle  
    BITMAPFILEHEADER hdr;       // bitmap file-header  
    PBITMAPINFOHEADER pbih;     // bitmap info-header  
    LPBYTE lpBits;              // memory pointer  
    DWORD dwTotal;              // total count of bytes  
    DWORD cb;                   // incremental count of bytes  
    BYTE *hp;                   // byte pointer  
    DWORD dwTmp; 

    pbih = (PBITMAPINFOHEADER) pbi; 
    lpBits = (LPBYTE) GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);

    if (!lpBits) 
	{cout<<"Error "<<"GlobalAlloc"<<endl;return false;}//errhandler("GlobalAlloc", hwnd); 

    // Retrieve the color table (RGBQUAD array) and the bits  
    // (array of palette indices) from the DIB.  
    if (!GetDIBits(hDC, hBMP, 0, (WORD) pbih->biHeight, lpBits, pbi, 
        DIB_RGB_COLORS)) 
    {
         cout<<"Error "<<"GlobalAlloc"<<endl;return false;//errhandler("GetDIBits", hwnd); 
    }

    // Create the .BMP file.  
    hf = CreateFile(pszFile, 
                   GENERIC_READ | GENERIC_WRITE, 
                   (DWORD) 0, 
                    NULL, 
                   CREATE_ALWAYS, 
                   FILE_ATTRIBUTE_NORMAL, 
                   (HANDLE) NULL); 
    if (hf == INVALID_HANDLE_VALUE) 
	{cout<<"Error "<<"CreateFile"<<endl;return false;}//errhandler("CreateFile", hwnd); 
    hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"  
    // Compute the size of the entire file.  
    hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + 
                 pbih->biSize + pbih->biClrUsed 
                 * sizeof(RGBQUAD) + pbih->biSizeImage); 
    hdr.bfReserved1 = 0; 
    hdr.bfReserved2 = 0; 

    // Compute the offset to the array of color indices.  
    hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + 
                    pbih->biSize + pbih->biClrUsed 
                    * sizeof (RGBQUAD); 

    // Copy the BITMAPFILEHEADER into the .BMP file.  
    if (!WriteFile(hf, (LPVOID) &hdr, sizeof(BITMAPFILEHEADER), 
        (LPDWORD) &dwTmp,  NULL)) 
    {
        cout<<"Error "<<"WriteFile"<<endl;return false;//errhandler("WriteFile", hwnd); 
    }

    // Copy the BITMAPINFOHEADER and RGBQUAD array into the file.  
    if (!WriteFile(hf, (LPVOID) pbih, sizeof(BITMAPINFOHEADER) 
                  + pbih->biClrUsed * sizeof (RGBQUAD), 
                  (LPDWORD) &dwTmp, ( NULL)))
	{ cout<<"Error "<<"WriteFile"<<endl;return false;}//errhandler("WriteFile", hwnd); 

    // Copy the array of color indices into the .BMP file.  
    dwTotal = cb = pbih->biSizeImage; 
    hp = lpBits; 
    if (!WriteFile(hf, (LPSTR) hp, (int) cb, (LPDWORD) &dwTmp,NULL)) 
	{cout<<"Error "<<"WriteFile"<<endl;return false;}//errhandler("WriteFile", hwnd); 

    // Close the .BMP file.  
     if (!CloseHandle(hf)) 
	 {  cout<<"Error "<<"CloseHandle"<<endl;return false;}//errhandler("CloseHandle", hwnd); 

    // Free memory.  
    GlobalFree((HGLOBAL)lpBits);

	return true;
}

//bool SaveBMPFile(const wchar_t *filename, HBITMAP bitmap, HDC bitmapDC, int width, int height/*,BYTE * image,int image_size*/){
//	bool Success=0;
//	HDC SurfDC=NULL;
//	HBITMAP OffscrBmp=NULL;
//	HDC OffscrDC=NULL;
//	LPBITMAPINFO lpbi=NULL;
//	LPVOID lpvBits=NULL;
//	HANDLE BmpFile=INVALID_HANDLE_VALUE;
//	BITMAPFILEHEADER bmfh;
//	if ((OffscrBmp = CreateCompatibleBitmap(bitmapDC, width, height)) == NULL)
//		return 0;
//	if ((OffscrDC = CreateCompatibleDC(bitmapDC)) == NULL)
//		return 0;
//	HBITMAP OldBmp = (HBITMAP)SelectObject(OffscrDC, OffscrBmp);
//	BitBlt(OffscrDC, 0, 0, width, height, bitmapDC, 0, 0, SRCCOPY);
//	if ((lpbi = (LPBITMAPINFO)(new char[sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD)])) == NULL) 
//		return 0;
//	ZeroMemory(&lpbi->bmiHeader, sizeof(BITMAPINFOHEADER));
//	lpbi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
//	SelectObject(OffscrDC, OldBmp);
//	if (!GetDIBits(OffscrDC, OffscrBmp, 0, height, NULL, lpbi, DIB_RGB_COLORS))
//		return 0;
//	if ((lpvBits = new char[lpbi->bmiHeader.biSizeImage]) == NULL)
//		return 0;
//	if (!GetDIBits(OffscrDC, OffscrBmp, 0, height, lpvBits, lpbi, DIB_RGB_COLORS))
//		return 0;
//	if ((BmpFile = CreateFile((LPWSTR)filename,
//						GENERIC_WRITE,
//						0, NULL,
//						CREATE_ALWAYS,
//						FILE_ATTRIBUTE_NORMAL,
//						NULL)) == INVALID_HANDLE_VALUE)
//		return 0;
//	DWORD Written;
//	bmfh.bfType = 19778;
//	bmfh.bfReserved1 = bmfh.bfReserved2 = 0;
//	if (!WriteFile(BmpFile, &bmfh, sizeof(bmfh), &Written, NULL))
//		return 0;
//	if (Written < sizeof(bmfh)) 
//		return 0; 
//	if (!WriteFile(BmpFile, &lpbi->bmiHeader, sizeof(BITMAPINFOHEADER), &Written, NULL)) 
//		return 0;
//	if (Written < sizeof(BITMAPINFOHEADER)) 
//		return 0;
//	int PalEntries;
//	if (lpbi->bmiHeader.biCompression == BI_BITFIELDS) 
//		PalEntries = 3;
//	else PalEntries = (lpbi->bmiHeader.biBitCount <= 8) ?
//					  (int)(1 << lpbi->bmiHeader.biBitCount) : 0;
//	if(lpbi->bmiHeader.biClrUsed) 
//	PalEntries = lpbi->bmiHeader.biClrUsed;
//	if(PalEntries){
//	if (!WriteFile(BmpFile, &lpbi->bmiColors, PalEntries * sizeof(RGBQUAD), &Written, NULL)) 
//		return 0;
//		if (Written < PalEntries * sizeof(RGBQUAD)) 
//			return 0;
//	}
//	bmfh.bfOffBits = GetFilePointer(BmpFile);
//	if (!WriteFile(BmpFile, lpvBits, lpbi->bmiHeader.biSizeImage, &Written, NULL)) 
//		return 0;
//	if (Written < lpbi->bmiHeader.biSizeImage) 
//		return 0;
//	bmfh.bfSize = GetFilePointer(BmpFile);
//	SetFilePointer(BmpFile, 0, 0, FILE_BEGIN);
//	if (!WriteFile(BmpFile, &bmfh, sizeof(bmfh), &Written, NULL))
//		return 0;
//	if (Written < sizeof(bmfh)) 
//		return 0;
//
//	CloseHandle(BmpFile);
//
//	return 1;
//}

BYTE* ConvertRGBToBMPBuffer ( BYTE* Buffer, int width, int height, long* newsize )
{

	// first make sure the parameters are valid
	if ( ( NULL == Buffer ) || ( width == 0 ) || ( height == 0 ) )
		return NULL;

	// now we have to find with how many bytes
	// we have to pad for the next DWORD boundary	

	int padding = 0;
	int scanlinebytes = width * 3;
	while ( ( scanlinebytes + padding ) % 4 != 0 )     // DWORD = 4 bytes
		padding++;
	// get the padded scanline width
	int psw = scanlinebytes + padding;
	
	// we can already store the size of the new padded buffer
	*newsize = height * psw;

	// and create new buffer
	BYTE* newbuf = new BYTE[*newsize];
	
	// fill the buffer with zero bytes then we dont have to add
	// extra padding zero bytes later on
	memset ( newbuf, 0, *newsize );

	// now we loop trough all bytes of the original buffer, 
	// swap the R and B bytes and the scanlines
	long bufpos = 0;   
	long newpos = 0;
	for ( int y = 0; y < height; y++ )
		for ( int x = 0; x < 3 * width; x+=3 )
		{
			bufpos = y * 3 * width + x;     // position in original buffer
			newpos = ( height - y - 1 ) * psw + x;           // position in padded buffer

			newbuf[newpos] = Buffer[bufpos+2];       // swap r and b
			newbuf[newpos + 1] = Buffer[bufpos + 1]; // g stays
			newbuf[newpos + 2] = Buffer[bufpos];     // swap b and r
		}

	return newbuf;
}

float getIntensity(float value)
{
	if(value > 1.0f)value = 1.0f;
	else if(value < 0.0f)value = 0.0f;
	return value;
}


bool ScreenCapture(Image * image,int x, int y, int width, int height, const wchar_t *filename){

	HDC hDc = GetDC(0);//CreateCompatibleDC(0);    
	HBITMAP hBmp = CreateCompatibleBitmap(GetDC(0), width, height);   
	
	bool ret = false;

	long * new_size = new long(0);
	/*RGBTRIPLE*/BYTE  * rgbdata = new BYTE/*RGBTRIPLE*/ [HEIGHT * WIDTH * 3];

	int i = 0;
	float r=0,g=0,b=0;
	int ir=0,ig=0,ib=0;
	for(int yy = 0;yy < HEIGHT;yy++)
	{
		for(int xx = 0;xx < (WIDTH);xx++)
		{
			r = getIntensity(image->getPixel(xx,(HEIGHT - 1)-yy).r);
			g = getIntensity(image->getPixel(xx,(HEIGHT - 1)-yy).g);
			b = getIntensity(image->getPixel(xx,(HEIGHT - 1)-yy).b);

			ir = r * 254;
			ig = g * 254;
			ib = b * 254;
			rgbdata[i]/*.rgbtRed*/ = /*(unsigned char)*/ir;		
			rgbdata[i + 1]/*.rgbtGreen*/ = /*(unsigned char)*/ig;		
			rgbdata[i + 2]/*.rgbtBlue*/ = /*(unsigned char)*/ib;		
			i = i + 3; 
		}
	}

	SelectObject(hDc, hBmp); 

	BYTE* buffer = ConvertRGBToBMPBuffer(rgbdata,WIDTH,HEIGHT,new_size);

	BITMAPINFO  pbmi;

	pbmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
    pbmi.bmiHeader.biWidth = WIDTH; 
    pbmi.bmiHeader.biHeight = HEIGHT; 
    pbmi.bmiHeader.biPlanes = 1; 
    pbmi.bmiHeader.biBitCount = 24; 
    pbmi.bmiHeader.biClrUsed = 1; 
    pbmi.bmiHeader.biCompression = BI_RGB; 
    pbmi.bmiHeader.biSizeImage = sizeof(BYTE) * *new_size/*3 * HEIGHT * WIDTH*/; 
    pbmi.bmiHeader.biClrImportant = 1; 
	pbmi.bmiHeader.biXPelsPerMeter=2400; 
	pbmi.bmiHeader.biYPelsPerMeter=2400; 
	/*pbmi.bmiColors->rgbRed = 0;
	pbmi.bmiColors->rgbGreen = 0;
	pbmi.bmiColors->rgbBlue = 0;
	pbmi.bmiColors->rgbReserved = 0;*/

	hBmp = CreateDIBitmap(hDc,&pbmi.bmiHeader,CBM_INIT,buffer,&pbmi,DIB_PAL_COLORS);
	//BitBlt(hDc, 0, 0, width, height, GetDC(0), x, y, SRC);  
	if(hBmp == NULL){cout<<"Error CreateDIBITMAP"<<endl;DeleteObject(hBmp);return false;}

	PBITMAPINFO newpbmi = CreateBitmapInfoStruct(NULL,hBmp);

	ret = CreateBMPFile(NULL,(LPTSTR)filename,&pbmi,hBmp,hDc);

	//bool ret = SaveBMPFile(filename, hBmp, hDc, width, height); 

	DeleteObject(hBmp);  
	return ret;
}

void writeBMP(Image * image,const wchar_t * filename)
{
	if(!ScreenCapture(image,0,0,WIDTH,HEIGHT,filename/*"BILD.BMP"*/))
	{
		cout << "Error creating image file!" << endl;
	}else
	{
		cout << "Success creating image file!" << endl;
	}
}