#pragma comment(lib,"glew32d.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#define _USE_MATH_DEFINES

using namespace std;


#include <Windows.h>

#include <GLheader/glew.h>

#include <gl/gl.h>
#include <gl/glu.h>

#include<vector>
#include<iostream>
#include<math.h>
#include <time.h>
#include <sstream>
#include <tchar.h>

#include <stdio.h>
#include <io.h>
#include <fcntl.h>

#include "algebra.h"
#include "utilities.h"
#include "mesh.h"

#include "Vec3.h"
#include "Image.h"
#include "Ray.h"
#include "mesh.h"
#include "Sphere.h"
#include "Camera.h"
#include "Light.h"
#include "bitmap.h"
#include "Scene.h"
#include "RayTracer.h"

#define TIMERID	100  /*SetTimer id*/
#define DEFAULT_STACK_SIZE	1024
#define UPDATE_FREQ 10

#define NUM_OF_LIGHTS 1

#define WIDTH 600
#define HEIGHT 600

int scr_width = WIDTH;
int scr_height = HEIGHT;

HGLRC hRC;
HDC hDc;

bool useRayTracer;

long numberOfTests;
wchar_t * imageFile;
bool useOpenGl;
bool usePerspective;
bool useAA;
int recDepth;

//GLuint programToon = 0;
//GLuint programPhong = 0;

//float ambient1[]  = {0.0,0.0,0.75,1};  // Gray (constant factor)
//float diffuse1[]  = {0.0,0.0,0.75,1};  // White (all directions)
//float specular1[] = {0.75,0.75,0.75,1};
//float LightPosition1[] = {0.0f, 0.0f, 0.0f, 1.0f};  // behind camera (relative to camera)
//
//float ambient2[]  = {0.0f, 0.0f, 0.0f, 1.0f};  // Gray (constant factor)
//float diffuse2[]  = {0.0f, 0.0f, 0.9f, 1.0f};  // White (all directions)
//float specular2[] = {0.1f, 0.1f, 0.1f, 1.0f};
//float LightPosition2[] = {-100.0f, 0.0f, 0.0f, 1.0f};  // behind camera (relative to camera)

//rtype renderType;
//rtype toonphongRender;



SimpleRayTracer * rayTracer;

//float ambientBlack[]  = {0.0,0.0,0.0,1};  // Gray (constant factor)
//float diffuseBlack[]  = {0.0,0.0,0.0,1};  // White (all directions)
//float specularBlack[] = {0.0,0.0,0.0,1};
//
//float ambientm[]  = {0.0,0.0,0.75,1};
//float diffusem[]  = {0.0,0.0,0.75,1};
//float specualrm[]  = {0.75,0.75,0.75,1};

float ambient1[]  = {0.0,0.0,0.75,1};  // Gray (constant factor)
float diffuse1[]  = {0.0,0.0,0.75,1};  // White (all directions)
float specular1[] = {0.75,0.75,0.75,1};
float LightPosition1[] = {0.0f, 0.0f, 0.0f, 1.0f};  // behind camera (relative to camera)

float ambient2[]  = {0.0f, 0.0f, 0.0f, 1.0f};  // Gray (constant factor)
float diffuse2[]  = {0.0f, 0.0f, 0.9f, 1.0f};  // White (all directions)
float specular2[] = {0.1f, 0.1f, 0.1f, 1.0f};
float LightPosition2[] = {-100.0f, 0.0f, 0.0f, 1.0f};  // behind camera (relative to camera)


//

void solidSphere(GLdouble radius, GLint slices, GLint stacks)
{
    glBegin(GL_LINE_LOOP);
    GLUquadricObj* quadric = gluNewQuadric();
  
   gluQuadricDrawStyle(quadric, GLU_LINE);
   gluSphere(quadric, radius, slices, stacks);
  
   gluDeleteQuadric(quadric);
   glEnd();
}


void display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	//glClear( GL_COLOR_BUFFER_BIT );

	if(useRayTracer)
	{
		//glUseProgram(0);

		/*glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHT1);
		*/

		std::ostringstream sstream;

		//glClear(GL_COLOR_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		rayTracer->numberOfTests = 0;
		rayTracer->recDepth = recDepth;
		rayTracer->useAA = useAA;

		cout << "Using: ";
		if(useAA)cout << "AA x4 ";
		cout << endl;

		clock_t begin=clock();
		
		rayTracer->fireRays(); //Start the raytracing!

		clock_t end=clock();

		glFlush();

		sstream << diffclock(end,begin);

		numberOfTests = rayTracer->numberOfTests;

		string rt = sstream.str();
		sstream << numberOfTests;
		string nt = sstream.str();

		std::string str = "Time: " + rt + " se" + " Num of tests :" + nt;  //or use glutSetWindowTitle(str.c_str());
		
		numberOfTests = 0;
		//cout << "Time: " << diffclock(end,begin) << " se" << " Num of tests :" << rayTracer->numberOfTests << endl; 
		cout << str << endl;
	
		useRayTracer = false;

	}else
	{
		//glEnable(GL_LIGHTING);

		//glEnable(GL_LIGHT0);

		//glEnable(GL_LIGHT1);

		Matrix m;

		glMatrixMode(GL_MODELVIEW);

		m = RotationZ(-rayTracer->getScene()->cam.Rot()->z);
		m = MatMatMul(m,RotationY(-rayTracer->getScene()->cam.Rot()->y));
		m = MatMatMul(m,RotationX(-rayTracer->getScene()->cam.Rot()->x));
		m = MatMatMul(m,Translation(-rayTracer->getScene()->cam.Pos()->x, -rayTracer->getScene()->cam.Pos()->y, -rayTracer->getScene()->cam.Pos()->z));

		glLoadMatrixd(m.e);
		//glLightfv(GL_LIGHT0,GL_POSITION, LightPosition1);

		

		for(int i = 0; i < int(rayTracer->getScene()->spheres.size());i++)
		{
			Sphere mesh = rayTracer->getScene()->spheres[i];

			glPushMatrix();

			m = Translation(mesh.GetPosition().x, mesh.GetPosition().y, mesh.GetPosition().z);
			///m = MatMatMul(m, RotationZ(/*mesh->rotation.z*/0.0f));
			//m = MatMatMul(m, RotationY(/*mesh->rotation.y*/0.0f));
			//m = MatMatMul(m, RotationX(/*mesh->rotation.x*/0.0f));
			//m = MatMatMul(m,Scale(1.0f,1.0f,1.0f));

			glMultMatrixd(m.e);

			//glUseProgram(programPhong);

			glColor4f(rayTracer->getScene()->spheres[i].color.r, rayTracer->getScene()->spheres[i].color.g, rayTracer->getScene()->spheres[i].color.b, 1.0f);

			solidSphere(rayTracer->getScene()->spheres[i].r,32,32);
			//glutSolidSphere(rayTracer->getScene()->spheres[i].r,32,32);

			glPopMatrix();
		}

		glFlush();
		
	}
}




void keypress(char key/*, int x, int y*/) {
	int num = 0;
	Vec3f * pos;
	switch(key) {
	case 'w': //FRONT
		moveZ(rayTracer->getScene()->cam.Pos(),rayTracer->getScene()->cam.Rot(),-1);
		break;
	case 'a': //LEFT
		moveX(rayTracer->getScene()->cam.Pos(),rayTracer->getScene()->cam.Rot(),-1);
		break;
	case 's': //BACK
		moveZ(rayTracer->getScene()->cam.Pos(),rayTracer->getScene()->cam.Rot(),1);
		break;
	case 'd': //RIGHT
		moveX(rayTracer->getScene()->cam.Pos(),rayTracer->getScene()->cam.Rot(),1);
		break;
	case 'W':  //UP
		moveY(rayTracer->getScene()->cam.Pos(),rayTracer->getScene()->cam.Rot(),-1);
		break;
	case 'S': //DOWN
		moveY(rayTracer->getScene()->cam.Pos(),rayTracer->getScene()->cam.Rot(),1);
		break;
	case 'q': //Rotation X
		pos = rayTracer->getScene()->cam.Rot();
		pos->x = pos->x + 0.4f;
		rayTracer->getScene()->cam.Print();
		break;
	case 'e': //Rotation Y
		pos = rayTracer->getScene()->cam.Rot();
		pos->y = pos->y + 0.4f;
		rayTracer->getScene()->cam.Print();
		break;
	case 'Q': //Rotation X
		pos = rayTracer->getScene()->cam.Rot();
		pos->x = pos->x - 0.4f;
		rayTracer->getScene()->cam.Print();
		break;
	case 'E': //Rotation Y
		pos = rayTracer->getScene()->cam.Rot();
		pos->y = pos->y - 0.4f;
		rayTracer->getScene()->cam.Print();
		break;
	case 'A': //Rotation Z
		pos = rayTracer->getScene()->cam.Rot();
		pos->z = pos->z - 0.4f;
		//rayTracer->getScene()->cam.Print();
		break;
	case 'D': //Rotation Z
		pos = rayTracer->getScene()->cam.Rot();
		pos->z = pos->z + 0.4f;
		//rayTracer->getScene()->cam.Print();
		break;
	case 'g': //Save image file
		cout << "Saving image file. " /*+ imageFile*/ << endl;
		writeBMP(rayTracer->getImage(),imageFile);//writeImage = true;
		
		break; 
	case 'p'://Start RayTracer
		useRayTracer = ! useRayTracer;
		break;
	case 'i'://Use/UnUse Antialiasing
		useAA = ! useAA;
		break;

	}

	//glutReshapeWindow(WIDTH,HEIGHT);

	//glutPostRedisplay();
}

void initTheRenderer(void)
{
	//GLenum err;

	//cout<<"Initializing the Renderer!"<<endl;

	useRayTracer = false;
	useAA = false;

	recDepth = 1;

	//glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGB);
	//glutInitWindowSize(WIDTH, HEIGHT);
	//glutCreateWindow("SimpleRayTracer");

	//glutDisplayFunc(display);
	//glutReshapeFunc(changeSize);
	//glutKeyboardFunc(keypress);

	//glClearColor(0.0f, 1.0f, 0.0f, 1.0f);

	Scene * scene = new Scene;

	

	numberOfTests = 0;

	//writeImage = false;

	imageFile = L"stuff/renderedimage.bmp";

	scene->cam = Camera(Vec3f(0,0,0),Vec3f(0.0f,0.0f,0.0f),90,1000,-5,WIDTH,HEIGHT,Vec3f(0,0,-1),Vec3f(0,1,0),Vec3f(-1,0,0));

	Image * image = new Image(WIDTH, HEIGHT);	

	rayTracer = new SimpleRayTracer(scene, image);

	//Setup the scene

	/*scene->add(Sphere(Vec3f(-2.0f, -2.0f, -10.2f), 1.0f,Vec3f(1.0f, 0.0f, 0.0f),0.25f));
	scene->spheres[0].setupMaterial(0.5f,1.0f,0.75f);*/

	scene->load("stuff/scene.txt");

	//Setup the lightning
	scene->lights[0] = Light(Vec3f(4.0f, 4.0f, -2.0f),Vec3f(0.05f,.05f,.05f),Vec3f(0.75f,0.5f,0.5f),Vec3f(.75f,.5f,.5f));

	//scene->lights[1] = Light(Vec3f(-4.0f, -4.0f, -1.0f),Vec3f(0.05f,.05f,.05f),Vec3f(0.75f,0.5f,0.5f),Vec3f(.75f,.5f,.5f));


	glEnable(GL_CULL_FACE);	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	//glColor3f(0.0f, 0.0f, .75f);

	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST); //Smooth polygons
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); //Best perspective corrections
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST); //Smooth points
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST); //Smooth lines

	

}

void changeSize(int w, int h) {

	if(useRayTracer){
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0, w, 0, h);
		glViewport(0,0,w,h);
	}else{
		Matrix m;
		glMatrixMode(GL_PROJECTION);
		m =	Perspective(50, w/(float)h, 1, 1000);
		glLoadMatrixd(m.e);
		glViewport(0,0,w,h);
	}
}


void changeSizeOpenGl(int w, int h) {

	/*if(useRayTracer){
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0, w, 0, h);
		glViewport(0,0,w,h);
	}else{*/
		Matrix m;
		glMatrixMode(GL_PROJECTION);
		m =	Perspective(50, w/(float)h, 1, 500);
		glLoadMatrixd(m.e);

		glViewport(0,0,w,h);

		//SwapBuffers( hDc);
	//}
}

// Function Declarations
//LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
//void EnableOpenGL(HWND hWnd, HDC * hDC, HGLRC * hRC);
//void DisableOpenGL(HWND hWnd, HDC hDC, HGLRC hRC);

//EnableOpenGL and DisableOpenGL found from ::http://www.nullterminator.net/opengl32.html
void EnableOpenGL(HWND hWnd, HDC * hDC, HGLRC * hRC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;

    //// get the device context (DC)
    *hDC = GetDC( hWnd );

    // set the pixel format for the DC
    ZeroMemory( &pfd, sizeof( pfd ) );
    pfd.nSize = sizeof( pfd );
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |
                  PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;
    iFormat = ChoosePixelFormat( *hDC, &pfd );
    SetPixelFormat( *hDC, iFormat, &pfd );

    // create and enable the render context (RC)
    *hRC = wglCreateContext( *hDC );
    wglMakeCurrent( *hDC, *hRC );
}

void DisableOpenGL(HWND hWnd, HDC * hDC, HGLRC * hRC)
{
    wglMakeCurrent( NULL, NULL );
    wglDeleteContext( *hRC );
    ReleaseDC( hWnd, *hDC );
}

HWND windowCreate (HINSTANCE hPI, HINSTANCE hI, int ncs, char *title, WNDPROC callbackFunc, int bgcolor) {

  HWND hWnd;
  WNDCLASS wc; 

  /* initialize and create the presentation window        */
  /* NOTE: The only important thing to you is that we     */
  /*       associate the function 'MainWndProc' with this */
  /*       window class. This function will be called by  */
  /*       windows when something happens to the window.  */
  if( !hPI) {
	 wc.lpszClassName = (LPWSTR)"GenericAppClass";
	 wc.lpfnWndProc = callbackFunc;          /* (this function is called when the window receives an event) */
	 wc.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;
	 wc.hInstance = hI;
	 wc.hIcon = LoadIcon( NULL, IDI_APPLICATION );
	 wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	 wc.hbrBackground = (HBRUSH) bgcolor;
	 wc.lpszMenuName = (LPWSTR)"GenericAppMenu";

	 wc.cbClsExtra = 0;
	 wc.cbWndExtra = 0;

	 RegisterClass( &wc );
  }

  /* NOTE: This creates a window instance. Don't bother about the    */
  /*       parameters to this function. It is sufficient to know     */
  /*       that this function creates a window in which we can draw. */
  hWnd = CreateWindow( (LPWSTR)"GenericAppClass",
				 L"3D Window",
				 WS_OVERLAPPEDWINDOW,
				 0,
				 0,
				 WIDTH,
				 HEIGHT,
				 /*CW_USEDEFAULT,
				 CW_USEDEFAULT,*/
				 NULL,
				 NULL,
				 hI,
				 NULL
				 );

  /* NOTE: This makes our window visible. */
  ShowWindow( hWnd, ncs );
  /* (window creation complete) */

  return hWnd;
}

void windowRefreshTimer (HWND hWnd, int updateFreq) {

  if(SetTimer(hWnd, TIMERID, updateFreq, NULL) == 0) {
	 /* NOTE: Example of how to use MessageBoxes, see the online help for details. */
	 MessageBox(NULL, L"Failed setting timer", L"Error!!", MB_OK);
	 exit (1);
  }
}

float theta = 0.0f;

LRESULT CALLBACK MainWndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam ) {
  
	//PAINTSTRUCT ps;
	//HANDLE context;
	//RECT rect;
	//static DWORD color = 0;

	/*rect.bottom = 600;
	rect.left	= 0;
	rect.right	= 800;
	rect.top	= 0;*/
  
	switch( msg ) {
		case WM_CREATE:       
			windowRefreshTimer (hWnd, UPDATE_FREQ);

			

			break;   
							
		case WM_TIMER:

			changeSizeOpenGl(scr_width,scr_height);display();
			SwapBuffers( hDc);
		
			//InvalidateRect( hWnd, NULL, TRUE);
			//
			//display();

			break;
		case WM_CHAR:
			keypress(wParam/*, int x, int y*/);
			break;
		case WM_SIZE:
			changeSizeOpenGl(scr_width = LOWORD(lParam),scr_height = HIWORD(lParam));
			SwapBuffers( hDc);
			break;
		case WM_PAINT:
			//context = BeginPaint( hWnd, &ps ); /* (you can safely remove the following line of code) */
			////TextOut((HDC)context, 10, 10, "Hi World!!", 11 ); /* 13 is the string length */
			//EndPaint( hWnd, &ps );

			/*glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
			glClear( GL_COLOR_BUFFER_BIT );
			
			glPushMatrix();
			glRotatef( theta, 0.0f, 0.0f, 1.0f );
			glBegin( GL_TRIANGLES );
			glColor3f( 1.0f, 0.0f, 0.0f ); glVertex2f( 0.0f, 1.0f );
			glColor3f( 0.0f, 1.0f, 0.0f ); glVertex2f( 0.87f, -0.5f );
			glColor3f( 0.0f, 0.0f, 1.0f ); glVertex2f( -0.87f, -0.5f );
			glEnd();
			glPopMatrix();
			*/
			
			display();
			SwapBuffers( hDc);

			//UpdateWindow(hWnd);
			/*changeSize(WIDTH,HEIGHT);
			
			UpdateWindow(hWnd);*/
			//theta += 0.01f;
			//UpdateWindow(hWnd);
			break;
		case WM_DESTROY:
			PostQuitMessage( 0 );
			DisableOpenGL(hWnd,&hDc,&hRC);
			DestroyWindow( hWnd );
			break;
		default:
			return( DefWindowProc( hWnd, msg, wParam, lParam )); 
   }

   return 0;
}

//#include "./models/mesh_bunny.h"
#include "./models/mesh_cow.h"
#include "./models/mesh_cube.h"
//#include "./models/mesh_frog.h"
#include "./models/mesh_knot.h"
#include "./models/mesh_sphere.h"
//#include "./models/mesh_teapot.h"
#include "./models/mesh_triceratops.h"

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow ) {

	HWND hWnd;
	MSG msg;
							/* Create the window, 3 last parameters important */
							/* The tile of the window, the callback function */
							/* and the backgrond color */

	hWnd = windowCreate (hPrevInstance, hInstance, nCmdShow, "3D Window", MainWndProc, COLOR_WINDOW+1);
	
	//Create a console window too, because it's handy!
	AllocConsole();
    HANDLE handle_out = GetStdHandle(STD_OUTPUT_HANDLE);
    int hCrt = _open_osfhandle((long) handle_out, _O_TEXT);
    FILE* hf_out = _fdopen(hCrt, "w");
    setvbuf(hf_out, NULL, _IONBF, 1);
    *stdout = *hf_out;

    HANDLE handle_in = GetStdHandle(STD_INPUT_HANDLE);
    hCrt = _open_osfhandle((long) handle_in, _O_TEXT);
    FILE* hf_in = _fdopen(hCrt, "r");
    setvbuf(hf_in, NULL, _IONBF, 128);
    *stdin = *hf_in;

	cout<<"Testing..";
	//--------------------------------------------//

	//hDc = GetDC( hWnd );
	
	EnableOpenGL(hWnd,&hDc,&hRC);

	if(glewInit() == GLEW_OK){

		cout << "GLEW is OK!";
		//programToon = initShader("shaders\\toonvert","shaders\\toonfrag");
		//programPhong = initShader("shaders\\phongvert","shaders\\phongfrag");
	}
	else
	{
		cout << "GLEW is not OK!\nExiting..";/*free(message)*/;exit(1);
	}

	initTheRenderer();

	changeSizeOpenGl(scr_width,scr_height);SwapBuffers( hDc);display();
	SwapBuffers( hDc);

	while( GetMessage( &msg, NULL, 0, 0 ) ) {
		TranslateMessage( &msg );
		DispatchMessage( &msg );
	}

	return msg.wParam;
}
