#ifndef _RAYTRACER_H_
#define _RAYTRACER_H_

#define _USE_MATH_DEFINES
#include<vector>
#include<iostream>
#include<math.h>
#include <time.h>
#include <sstream>
//#include <fstream>
//#include <windows.h>
#include <tchar.h>


//#include "GL/glew.h"
//#include "GL/glut.h"

#include "algebra.h"
#include "utilities.h"
#include "mesh.h"

#include "Vec3.h"
#include "Image.h"
#include "Ray.h"
#include "Sphere.h"
#include "Camera.h"
#include "Light.h"
#include "bitmap.h"
#include "Scene.h"



class SimpleRayTracer {
private: 
	Scene * scene;
	Image * image;

	

	Vec3f getEyeRayDirection(float x, float y) {

		float rotx = toRadians(scene->cam.Rot()->x);//scene->cam.Rot()->x * M_PI / 180;
		float roty = toRadians(scene->cam.Rot()->y);//scene->cam.Rot()->y * M_PI / 180;
		float rotz = toRadians(scene->cam.Rot()->z);//scene->cam.Rot()->y * M_PI / 180;
		float z = -5.0f;	

		float sizeX = 4.0f; 
		float sizeY = 4.0f; 

		float left = -sizeX  * 0.5f;
		float bottom = -sizeY * 0.5f;

		float fovW =  50.0f * M_PI / 180.0f;
		float fovH = (float)sizeY / (float)sizeX * fovW;
		float tanFovW = tan(fovW);
		float tanFovH = tan(fovH);

		float dx =  sizeX / (float(image->getWidth()));  
		float dy =  sizeY / (float(image->getHeight()));

		float dirx = (bottom + x * dx) * tanFovW;
		float diry = (left + y * dy) * tanFovH ;
		
		Vec3f retVec = Vec3f(dirx, diry, z);
		RotateZ(retVec,rotz);
		RotateY(retVec,roty);
		RotateX(retVec,rotx);
		


		retVec.normalize();
		
		return retVec;

	}


public:

	long numberOfTests;
	bool useAA;
	int recDepth;

	SimpleRayTracer(Scene * scene, Image * image) {
		this->scene = scene;
		this->image = image;
	}

	Scene * getScene()
	{
		return scene;
	}

	Image * getImage()
	{
		return image;
	}

	void searchClosestHitReflection(const Ray & ray, HitRec & hitRec,int sp) {
		float len = 90000000000000000.0f;
		int ilen = scene->spheres.size(); 
		hitRec.anyHit = false;
		hitRec.tHit = 0.0f;
		HitRec temp;
		temp = hitRec;

		for (int i = 0; i < ilen; ++i) {

			if(scene->spheres[i].hit(ray, temp, numberOfTests) && i != sp)
			{
				if(temp.tHit < len){
					scene->spheres[i].computeSurfaceHitFields(ray,temp);
					temp.anyHit = true;
					temp.sphereHit = i;
					len = temp.tHit;
					hitRec = temp;
				}
			}
		}
	}

	Vec3f SearchReflectionHit(const Ray & ray, HitRec & hitRec) {

		if(scene->spheres[hitRec.sphereHit].reflection <= 0 || hitRec.primIndex <= 0)return Vec3f(0.0f,0.0f,0.0f);

		searchClosestHitReflection(ray,hitRec,hitRec.sphereHit);

		if(hitRec.anyHit)
		{
			HitRec refRec;
			Ray refRay;

			refRec.anyHit = false;
			refRec.primIndex = hitRec.primIndex - 1;
			refRec.sphereHit = hitRec.sphereHit;

			float rest = 1.0f -  scene->spheres[hitRec.sphereHit].reflection;

			hitRec.col =  SearchReflectionHit(refRay,refRec) * scene->spheres[hitRec.sphereHit].reflection + GetColorNoReflection(ray,hitRec,hitRec.primIndex - 1)  * rest;

			return hitRec.col;
		}

		return Vec3f(0.0f,0.0f,0.0f);
	}

	


//Serach the closest point in for the ray
bool searchClosestHit(const Ray & ray, HitRec & hitRec) {
		float len = 90000000000000000.0f;
		int ilen = scene->spheres.size(); 
		hitRec.anyHit = false;
		hitRec.tHit = 0.0f;
		HitRec temp;
		temp = hitRec;

		for (int i = 0; i < ilen; ++i) {

			if(scene->spheres[i].hit(ray, temp, numberOfTests))
			{
				if(temp.tHit < len){
					scene->spheres[i].computeSurfaceHitFields(ray,temp);
					temp.anyHit = true;
					temp.sphereHit = i;
					len = temp.tHit;
					hitRec = temp;
				}
			}
		}

		return hitRec.anyHit;
	}
//Research if a point is illuminated or not	
void searchHitToLight(const Ray & ray, HitRec & hitRec,float dist,int sp) {
		
		float len = 90000000000000000.0f;
		int ilen = scene->spheres.size();
		hitRec.anyHit = false;
		hitRec.tHit = 0.0f;

		HitRec temp;
		temp = hitRec;

		for (int i = 0; i < ilen; ++i) {

			if(scene->spheres[i].hit(ray, temp, numberOfTests) && i != sp)
			{
				if(temp.tHit < len){
					scene->spheres[i].computeSurfaceHitFields(ray,temp);
					temp.anyHit = true;
					temp.sphereHit = i;
					len = temp.tHit;
					hitRec = temp;
				}
			}
		}

		if(hitRec.anyHit)
		{
			if(hitRec.tHit > dist)
					hitRec.anyHit = false;

		}

	}



		
//Gets the color for the point
Vec3f GetPhongReflectionAndShadows(const Ray & ray,HitRec & hitRec/*,float x,float y*/)
	{
		Vec3f final_color = Vec3f(0.0f,0.0f,0.0f);

		Ray shadowRay;
		HitRec shadowRec;

		Ray refRay;
		HitRec refRec;

		Vec3f computed_color = Vec3f(scene->spheres[hitRec.sphereHit].color.r,scene->spheres[hitRec.sphereHit].color.g,scene->spheres[hitRec.sphereHit].color.b) * scene->spheres[hitRec.sphereHit].GetAmbient();

		for(int i = 0;i < NUM_OF_LIGHTS;i++)
		{
			Vec3f intesity = Vec3f(0.0f,0.0f,0.0f);
			shadowRay.d = (scene->lights[i].GetPosition() - hitRec.p).normalize();
			shadowRay.o = hitRec.p;
			shadowRec.anyHit = false;
			Vec3f shRay = shadowRay.d;
			Vec3f N = hitRec.n;
			Vec3f L = shRay;
			Vec3f V = - shRay;
			Vec3f R = Reflect(N,V);//N * 2 * (V.dot(N)) - V;
			Vec3f VV = - ray.d;
			Vec3f RR = Reflect(N,VV);//N * 2 * (VV.dot(N)) - VV;
			refRec.anyHit = false;
			refRec.col = Vec3f(0.0f,0.0f,0.0f);
			refRec.sphereHit = hitRec.sphereHit;
			refRay.d = RR;
			refRay.o = hitRec.p;
			refRec.primIndex = recDepth;
			float lambertTerm = N.dot(L);

			SearchReflectionHit(refRay,refRec);

			if(lambertTerm > 0.0f)
			{
				searchHitToLight(shadowRay,shadowRec,(scene->lights[i].GetPosition() - hitRec.p).len(),hitRec.sphereHit);
				intesity += scene->lights[i].GetDiffuse() * scene->spheres[hitRec.sphereHit].GetDiffuse() * lambertTerm;
				//float specular = pow( max(R.dot(L), 0.0f),100);
				float specular = pow( max(R.dot(ray.d), 0.0f),100);
				intesity += scene->lights[i].GetSpecular() * scene->spheres[hitRec.sphereHit].GetSpecular() * specular;

			}else
			{
				shadowRec.anyHit = true;
			}

			computed_color += scene->lights[i].GetAmbient();

			float rest = 1.0f - scene->spheres[hitRec.sphereHit].reflection;

			computed_color = computed_color * rest + refRec.col * scene->spheres[hitRec.sphereHit].reflection;
		
			if (shadowRec.anyHit) {
				computed_color *= 0.25;
			}else
			{
				computed_color += intesity; 
			}

			final_color += computed_color;

			computed_color = Vec3f(0.0f,0.0f,0.0f);

		}

		return final_color;

	}

Vec3f GetColorNoReflection(const Ray & ray,HitRec & hitRec,int depth)
{
		Vec3f final_color = Vec3f(0.0f,0.0f,0.0f);

		Ray shadowRay;
		HitRec shadowRec;

		Ray refRay;
		HitRec refRec;

		Vec3f computed_color = Vec3f(scene->spheres[hitRec.sphereHit].color.r,scene->spheres[hitRec.sphereHit].color.g,scene->spheres[hitRec.sphereHit].color.b) * scene->spheres[hitRec.sphereHit].GetAmbient();

		for(int i = 0;i < NUM_OF_LIGHTS;i++)
		{
			Vec3f intesity = Vec3f(0.0f,0.0f,0.0f);
			shadowRay.d = (scene->lights[i].GetPosition() - hitRec.p).normalize();
			shadowRay.o = hitRec.p;
			shadowRec.anyHit = false;
			Vec3f shRay = shadowRay.d;
			Vec3f N = hitRec.n;
			Vec3f L = shRay;
			Vec3f V = - shRay;
			Vec3f R = Reflect(N,V);//N * 2 * (V.dot(N)) - V;
			Vec3f VV = - ray.d;
			Vec3f RR = Reflect(N,VV);//N * 2 * (VV.dot(N)) - VV;
			refRec.anyHit = false;
			refRec.col = Vec3f(0.0f,0.0f,0.0f);
			refRec.sphereHit = hitRec.sphereHit;
			refRay.d = RR;
			refRay.o = hitRec.p;
			refRec.primIndex = depth;
			float lambertTerm = N.dot(L);

			SearchReflectionHit(refRay,refRec);

			if(lambertTerm > 0.0f)
			{
				searchHitToLight(shadowRay,shadowRec,(scene->lights[i].GetPosition() - hitRec.p).len(),hitRec.sphereHit);
				intesity += scene->lights[i].GetDiffuse() * scene->spheres[hitRec.sphereHit].GetDiffuse() * lambertTerm;
				//float specular = pow( max(R.dot(L), 0.0f),100);
				float specular = pow( max(R.dot(ray.d), 0.0f),100);
				intesity += scene->lights[i].GetSpecular() * scene->spheres[hitRec.sphereHit].GetSpecular() * specular;
			}else
			{
				shadowRec.anyHit = true;
			}

			computed_color += scene->lights[i].GetAmbient();

			float rest = 1.0f - scene->spheres[hitRec.sphereHit].reflection;

			
			computed_color = computed_color * rest + refRec.col * scene->spheres[hitRec.sphereHit].reflection;


			if (shadowRec.anyHit) {
				computed_color *= 0.25;
			}else
			{
				computed_color += intesity; 
			}

			final_color += computed_color;

			computed_color = Vec3f(0.0f,0.0f,0.0f);

		}

		return final_color;

}

	void fireRays(void) { 

		const float coef = 0.25f; 

		numberOfTests = 0;

		Ray ray;
		HitRec hitRec;Vec3f final_color;

		ray.o = Vec3f(scene->cam.Pos()->x, scene->cam.Pos()->y, scene->cam.Pos()->z);

		if(useAA)

		for (int y = 0; y < image->getHeight(); y++) {
			for (int x = 0; x < image->getWidth(); x++) {

				final_color = Vec3f(0.0f, 0.0f, 0.0f);  

				//4 x Supersampling loop
				for(float fx = x; fx < x + 1.0f; fx += 0.5f){
					for(float fy = y; fy < y + 1.0f; fy += 0.5f)
					{ 
						ray.d = getEyeRayDirection(fx, fy);

						hitRec.anyHit = false;

						if(searchClosestHit(ray, hitRec))//Find closest hitpoint
						{

							//Get the resulting color for that pixel
							final_color += GetPhongReflectionAndShadows(ray,hitRec/*,x,y*/) * coef;
						}
					}
				}
				//Finally set the color
				image->setPixel(x, y, final_color);
				glSetPixel(x, y, final_color);
			}
		}

		else

			for (int y = 0; y < image->getHeight(); y++) {
			for (int x = 0; x < image->getWidth(); x++) {

				final_color = Vec3f(0.0f, 0.0f, 0.0f);  

			
						ray.d = getEyeRayDirection(x, y);

						hitRec.anyHit = false;

						if(searchClosestHit(ray, hitRec))
						{
							final_color += GetPhongReflectionAndShadows(ray,hitRec/*,x,y*/);
						}


					image->setPixel(x, y, final_color);
				glSetPixel(x, y, final_color);
				}

				
				
				
			}
		}
	
};







#endif