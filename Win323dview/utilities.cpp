#include "utilities.h"

#include <Windows.h>
#include <GLheader/glew.h>
#include "mesh.h"

//const char* vertShaderSrc[] = {
//	"varying vec3 normal;"
//	"void main() {"
//	" gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;"
//	" normal = gl_Normal;"  
//	"}"
//};
//
//const char* fragShaderSrc[] = {
//	"varying vec3 normal;"
//	"void main() {"
//	" gl_FragColor = vec4(abs(normal), 1.0);"
//	"}"
//};

//void PrintRenderType(rtype r)
//{
//	switch(r)
//	{
//	case 0:
//		printf("Render:Wireframe\n");
//		break;
//	case 1:
//		printf("Render:Flatshading\n");
//		break;
//	case 2:
//		printf("Render:GouraudShading\n");
//		break;
//	case 3:
//		printf("Render:PhongShadong\n");
//		break;
//	case 4:
//		printf("Render:ToonShading\n");
//		break;
//	}
//}

string ReadTextFile(const char * filename)
{
	char c;
	FILE *file;
	file = fopen(filename, "r");
	string str = "";
	if (file) {
		while ((c = getc(file)) != EOF)
			str.insert(str.end(),c);
		fclose(file);
	}
	return str;
}

GLuint initShader(const char * vert, const char * frag) {
	
	int maxLength = 1024,i = 0;

	string ver,fra;

	GLint compiled, linked;

	GLuint shaderProgramId = 0;

	char * vertShaderSrc = NULL;
  	char * fragShaderSrc = NULL;

	GLuint vertShaderId = 0;
	GLuint fragShaderId = 0;

	ver = ReadTextFile(vert);
  	fra = ReadTextFile(frag);

	vertShaderSrc = new char [ver.size()+1];
	strcpy (vertShaderSrc, ver.c_str());

	fragShaderSrc = new char [fra.size()+1];
	strcpy (fragShaderSrc, fra.c_str());

	vertShaderId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertShaderId, 1, (const GLchar **)&vertShaderSrc, NULL);
	glCompileShader(vertShaderId);
	glGetShaderiv(vertShaderId, GL_COMPILE_STATUS, &compiled);

	if (!compiled) 
	{ 
		printf("Compile Error Vertex Shader:");
		printf("%s\n",vert);
	}

	fragShaderId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragShaderId, 1, (const GLchar **)&fragShaderSrc, NULL);
	glCompileShader(fragShaderId);
	glGetShaderiv(fragShaderId, GL_COMPILE_STATUS, &compiled);

	if (!compiled) { 
		printf("Compile Error Fragment Shader:");
		printf("%s\n",frag);
	}

	shaderProgramId = glCreateProgram();
	glAttachShader(shaderProgramId, vertShaderId);
	glAttachShader(shaderProgramId, fragShaderId);
	glLinkProgram(shaderProgramId);
	glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &linked);

	if (!linked) { 
		printf("Link Error Shader Program!\n"); 
	}

	free(vertShaderSrc);
	free(fragShaderSrc);

	return shaderProgramId;
	
}

void ClearScreen(){
    int n;
    for (n = 0; n < 10; n++)
      printf( "\n\n\n\n\n\n\n\n\n\n" );
}

//void calculateFlatNormals(Mesh *mesh)
//{
//	for(int i = 0;i < mesh->nt;i++)
//	{
//		int i0 = mesh->triangles[i].vInds[0];
//		int i1 = mesh->triangles[i].vInds[1];
//		int i2 = mesh->triangles[i].vInds[2];
//		mesh->triangles[i].norm = ComputeTriangleNormal(mesh->vertices[i0],mesh->vertices[i1],mesh->vertices[i2]);
//	}
//}
//
//void calculateGouraudNormals(Mesh *mesh)
//{
//	Vector n;
//	int i0 = 0;
//	int i1 = 0;
//	int i2 = 0;
// 
//	n = ZeroVector();
//
//	for(int i = 0;i < mesh->nv;i++)
//	{
//		//Find triangle normals
//		for(int j = 0;j < mesh->nt;j++)
//		{
//			i0 = mesh->triangles[j].vInds[0];
//			i1 = mesh->triangles[j].vInds[1];
//			i2 = mesh->triangles[j].vInds[2];
//
//			if( i == i0
//			|| i == i1
//			|| i == i2)
//			{
//				n = Add(n,mesh->triangles[j].norm);
//			}
//
//		}
//
//		mesh->vertexNormals[i] = Normalize(ScalarVecMul(1 / Length(n),n));
//
//		n = ZeroVector();
//	}
//
//}
//
//void renderGouraudShading(Mesh *mesh) {
//	glShadeModel(GL_SMOOTH);
//	glBegin(GL_TRIANGLES);
//	for(int i = 0;i < mesh->nt;i++)
//	{
//		int i0 = mesh->triangles[i].vInds[0];
//		int i1 = mesh->triangles[i].vInds[1];
//		int i2 = mesh->triangles[i].vInds[2];
//		
//		glNormal3dv((double *) &mesh->vertexNormals[i0]);
//		glVertex3dv((double *)&mesh->vertices[i0]);
//		glNormal3dv((double *) &mesh->vertexNormals[i1]);
//		glVertex3dv((double *)&mesh->vertices[i1]);
//		glNormal3dv((double *) &mesh->vertexNormals[i2]);
//		glVertex3dv((double *)&mesh->vertices[i2]);
//	}
//	glEnd();
//}
//
//void renderFlatShading(Mesh *mesh) {
//	
//	glShadeModel(GL_FLAT);
//	glBegin(GL_TRIANGLES);
//	for(int i = 0;i < mesh->nt;i++)
//	{
//		int i0 = mesh->triangles[i].vInds[0];
//		int i1 = mesh->triangles[i].vInds[1];
//		int i2 = mesh->triangles[i].vInds[2];
//		glNormal3dv((double *) & mesh->triangles[i].norm);
//		glVertex3dv((double *)&mesh->vertices[i0]);
//		glVertex3dv((double *)&mesh->vertices[i1]);
//		glVertex3dv((double *)&mesh->vertices[i2]);
//	}
//	glEnd();
//}
//
//void renderWireframe(Mesh *mesh) {
//
//	
//	for(int i=0; i<mesh->nt; i++) {
//		glBegin(GL_LINE_LOOP);
//		int i0 = mesh->triangles[i].vInds[0];
//		int i1 = mesh->triangles[i].vInds[1];
//		int i2 = mesh->triangles[i].vInds[2];
//		glVertex3dv((double *)&mesh->vertices[i0]);
//		glVertex3dv((double *)&mesh->vertices[i1]);
//		glVertex3dv((double *)&mesh->vertices[i2]);
//		glEnd();
//	}
//}

void RotateX(Vec3f & vec,float q)
{
	//y' = y*cos q - z*sin q
	//z' = y*sin q + z*cos q
	//x' = x

	float x = vec.x,y = vec.y,z = vec.z;

	vec.y = y * cosf(q) - z * sinf(q);
	vec.z = y * sinf(q) + z * cosf(q);
	vec.x = x;

}

void RotateY(Vec3f & vec,float q)
{
	//z' = z*cos q - x*sin q
	//x' = z*sin q + x*cos q
	//y' = y

	float x = vec.x,y = vec.y,z = vec.z;

	vec.z = z * cosf(q) - x * sinf(q);
	vec.x = z * sinf(q) + x * cosf(q);
	vec.y = y;


}

void RotateZ(Vec3f & vec,float q)
{
	//x' = x*cos q - y*sin q
	//y' = x*sin q + y*cos q 
	//z' = z

	float x = vec.x,y = vec.y,z = vec.z;

	vec.x = x * cosf(q) - y * sinf(q);
	vec.y = x * sinf(q) + y * cosf(q);
	vec.z = z;


}

/*
Used from: 3dkingdoms: Reflecting a vector

Link xhttp://www.3dkingdoms.com/weekly/weekly.php?a=2

*/
Vec3f Reflect(Vec3f N,Vec3f V)
{
	return N * 2 * (V.dot(N)) - V;
}


double diffclock(clock_t clock1,clock_t clock2)
{
	double diffticks= clock1 - clock2;
	double diffs= diffticks / CLOCKS_PER_SEC;
	return diffs;
}

float toRadians(float val)
{
	return val * M_PI / 180;
}

void glSetPixel(int x, int y, Vec3f & c) {

	glColor3f(c.r, c.g, c.b);
	glBegin(GL_POINTS);
	glVertex2i(x, y);
	glEnd();
}

void moveX(Vec3f * pos,Vec3f * rot,float v)
{
	Vec3f nPos = *pos;
	nPos.x += v;
	Vec3f dir = (nPos - *pos);

	//RotateZ(dir,toRadians(rot->z));
	RotateY(dir,toRadians(rot->y));
	RotateX(dir,toRadians(rot->x));
	
	
	//dir.normalize();

	nPos = *pos + dir; 

	pos->x = nPos.x;
	pos->y = nPos.y;
	pos->z = nPos.z;
}

void moveY(Vec3f * pos,Vec3f * rot,float v)
{
	Vec3f nPos = *pos;
	nPos.y += v;
	Vec3f dir = (nPos - *pos);

///RotateZ(dir,toRadians(rot->z));
	RotateY(dir,toRadians(rot->y));
	RotateX(dir,toRadians(rot->x));
	
	
	//dir.normalize();

	nPos = *pos + dir; 

	pos->x = nPos.x;
	pos->y = nPos.y;
	pos->z = nPos.z;

}

void moveZ(Vec3f * pos,Vec3f * rot,float v)
{
	Vec3f nPos = *pos;
	nPos.z += v;
	Vec3f dir = (nPos - *pos);

	//RotateZ(dir,toRadians(rot->z));
	RotateY(dir,toRadians(rot->y));
	RotateX(dir,toRadians(rot->x));

	//dir.normalize();

	nPos = *pos + dir; 

	pos->x = nPos.x;
	pos->y = nPos.y;
	pos->z = nPos.z;
}

/*
ExtractFrustum, SphereInFrustum
Used code samples from:	Frustum culling in OpenGl 
By Mark Morley, December 2000
Link: xhttp://www.crownandcutlass.com/features/technicaldetails/frustum.html
*/
void ExtractFrustum(double frustum[6][4])
{
	matrix proj;
	matrix modl;
	matrix clip;	
	double   t;

	glGetDoublev( GL_PROJECTION_MATRIX, proj.e );
	glGetDoublev( GL_MODELVIEW_MATRIX, modl.e );

   clip = MatMatMul(proj,modl);

   //RIGHT
   frustum[0][0] = clip.e[ 3] - clip.e[ 0];
   frustum[0][1] = clip.e[ 7] - clip.e[ 4];
   frustum[0][2] = clip.e[11] - clip.e[ 8];
   frustum[0][3] = clip.e[15] - clip.e[12];

   
   t = sqrt( frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2] );
   frustum[0][0] /= t;
   frustum[0][1] /= t;
   frustum[0][2] /= t;
   frustum[0][3] /= t;

   //LEFT
   frustum[1][0] = clip.e[ 3] + clip.e[ 0];
   frustum[1][1] = clip.e[ 7] + clip.e[ 4];
   frustum[1][2] = clip.e[11] + clip.e[ 8];
   frustum[1][3] = clip.e[15] + clip.e[12];

  
   t = sqrt( frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2] );
   frustum[1][0] /= t;
   frustum[1][1] /= t;
   frustum[1][2] /= t;
   frustum[1][3] /= t;

   //BOTTOM
   frustum[2][0] = clip.e[ 3] + clip.e[ 1];
   frustum[2][1] = clip.e[ 7] + clip.e[ 5];
   frustum[2][2] = clip.e[11] + clip.e[ 9];
   frustum[2][3] = clip.e[15] + clip.e[13];


   t = sqrt( frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2] );
   frustum[2][0] /= t;
   frustum[2][1] /= t;
   frustum[2][2] /= t;
   frustum[2][3] /= t;

   //TOP
   frustum[3][0] = clip.e[ 3] - clip.e[ 1];
   frustum[3][1] = clip.e[ 7] - clip.e[ 5];
   frustum[3][2] = clip.e[11] - clip.e[ 9];
   frustum[3][3] = clip.e[15] - clip.e[13];


   t = sqrt( frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2] );
   frustum[3][0] /= t;
   frustum[3][1] /= t;
   frustum[3][2] /= t;
   frustum[3][3] /= t;

   //FAR
   frustum[4][0] = clip.e[ 3] - clip.e[ 2];
   frustum[4][1] = clip.e[ 7] - clip.e[ 6];
   frustum[4][2] = clip.e[11] - clip.e[10];
   frustum[4][3] = clip.e[15] - clip.e[14];


   t = sqrt( frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2] );
   frustum[4][0] /= t;
   frustum[4][1] /= t;
   frustum[4][2] /= t;
   frustum[4][3] /= t;

   //NEAR
   frustum[5][0] = clip.e[ 3] + clip.e[ 2];
   frustum[5][1] = clip.e[ 7] + clip.e[ 6];
   frustum[5][2] = clip.e[11] + clip.e[10];
   frustum[5][3] = clip.e[15] + clip.e[14];


   t = sqrt( frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2] );
   frustum[5][0] /= t;
   frustum[5][1] /= t;
   frustum[5][2] /= t;
   frustum[5][3] /= t;
}

bool SphereInFrustum( double x, double y, double z, double radius,double frustum[6][4] )
{
   int p;

   for( p = 0; p < 6; p++ )
      if( frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] <= -radius )
         return false;
   return true;
}

void CalculateBoundingSphere(Mesh * mesh,int stride)
{
	//Find Center
	double count = mesh->nv;
	Vector * ptr = mesh->vertices;
	mesh->boundingSphere.center.x = 0;
	mesh->boundingSphere.center.y = 0;
	mesh->boundingSphere.center.z = 0;
	for (int i=0;i<mesh->nv;++i) {

		mesh->boundingSphere.center.x += ptr->x;
		mesh->boundingSphere.center.y += ptr->y;
		mesh->boundingSphere.center.z += ptr->z;
        ptr++;
    }
	mesh->boundingSphere.center.x /= (double)count;
	mesh->boundingSphere.center.y /= (double)count;
	mesh->boundingSphere.center.z /= (double)count;

	mesh->boundingSphere.radius = 0.0;

	ptr = mesh->vertices;

	//Find the farthest point
	for (int i=0;i<mesh->nv;++i) {

        Vector v;

        v= Subtract(*ptr,mesh->boundingSphere.center);

        double distSq=SquareLength(v);

        if (distSq > mesh->boundingSphere.radius)

            mesh->boundingSphere.radius=distSq;

        ptr++;

    }

    mesh->boundingSphere.radius = sqrt(mesh->boundingSphere.radius);
	

}

void calculateFlatNormals(Mesh *mesh)
{
	for(int i = 0;i < mesh->nt;i++)
	{
		int i0 = mesh->triangles[i].vInds[0];
		int i1 = mesh->triangles[i].vInds[1];
		int i2 = mesh->triangles[i].vInds[2];
		mesh->triangles[i].norm = ComputeTriangleNormal(mesh->vertices[i0],mesh->vertices[i1],mesh->vertices[i2]);
	}
}

void calculateGouraudNormals(Mesh *mesh)
{
	Vector n;
	int i0 = 0;
	int i1 = 0;
	int i2 = 0;
 
	n = ZeroVector();

	for(int i = 0;i < mesh->nv;i++)
	{
		//Find triangle normals
		for(int j = 0;j < mesh->nt;j++)
		{
			i0 = mesh->triangles[j].vInds[0];
			i1 = mesh->triangles[j].vInds[1];
			i2 = mesh->triangles[j].vInds[2];

			if( i == i0
			|| i == i1
			|| i == i2)
			{
				n = Add(n,mesh->triangles[j].norm);
			}

		}

		mesh->vertexNormals[i] = Normalize(ScalarVecMul(1 / Length(n),n));

		n = ZeroVector();
	}

}

void renderGouraudShading(Mesh *mesh) {
	glShadeModel(GL_SMOOTH);
	glBegin(GL_TRIANGLES);
	for(int i = 0;i < mesh->nt;i++)
	{
		int i0 = mesh->triangles[i].vInds[0];
		int i1 = mesh->triangles[i].vInds[1];
		int i2 = mesh->triangles[i].vInds[2];
		
		glNormal3dv((double *) &mesh->vertexNormals[i0]);
		glVertex3dv((double *)&mesh->vertices[i0]);
		glNormal3dv((double *) &mesh->vertexNormals[i1]);
		glVertex3dv((double *)&mesh->vertices[i1]);
		glNormal3dv((double *) &mesh->vertexNormals[i2]);
		glVertex3dv((double *)&mesh->vertices[i2]);
	}
	glEnd();
}

void renderFlatShading(Mesh *mesh) {
	
	glShadeModel(GL_FLAT);
	glBegin(GL_TRIANGLES);
	for(int i = 0;i < mesh->nt;i++)
	{
		int i0 = mesh->triangles[i].vInds[0];
		int i1 = mesh->triangles[i].vInds[1];
		int i2 = mesh->triangles[i].vInds[2];
		glNormal3dv((double *) & mesh->triangles[i].norm);
		glVertex3dv((double *)&mesh->vertices[i0]);
		glVertex3dv((double *)&mesh->vertices[i1]);
		glVertex3dv((double *)&mesh->vertices[i2]);
	}
	glEnd();
}

void renderWireframe(Mesh *mesh) {

	
	for(int i=0; i<mesh->nt; i++) {
		glBegin(GL_LINE_LOOP);
		int i0 = mesh->triangles[i].vInds[0];
		int i1 = mesh->triangles[i].vInds[1];
		int i2 = mesh->triangles[i].vInds[2];
		glVertex3dv((double *)&mesh->vertices[i0]);
		glVertex3dv((double *)&mesh->vertices[i1]);
		glVertex3dv((double *)&mesh->vertices[i2]);
		glEnd();
	}
}