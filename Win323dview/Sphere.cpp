#include "Sphere.h"
#include <iostream>


using namespace std;

/* Used sources:  
xhttp://wiki.cgsociety.org/index.php/Ray_Sphere_Intersection

xhttp://www.lighthouse3d.com/tutorials/maths/ray-sphere-intersection/
*/

bool Sphere::hit(const Ray & r, HitRec & rec, long & numberoftests) const {	

	Vec3f v = c - r.o;
	float s = v.dot(r.d);
	float vLenSq = v.dot(v);
	float radSq = this->r * this->r; 

	rec.tHit = 0.0f;

	if (s < 0 && vLenSq > radSq) {
		rec.anyHit =  false;
		return rec.anyHit;
	}

	float mSq = vLenSq - s * s;

	if (mSq > radSq) {
		rec.anyHit =  false;
		return rec.anyHit;
	}

	++numberoftests;
	Vec3f rc = r.o - c;

	float A = r.d.dot(r.d);
	float B = 2 * (rc).dot(r.d);
	float C = (rc).dot(rc) - radSq;
	float delta = B*B - 4 * A * C;

	float dSqrt;
	dSqrt = sqrtf(delta);

	float q;
    if (B < 0)
        q = (-B - dSqrt)/2.0f;
    else
        q = (-B + dSqrt)/2.0f;

    // compute t0 and t1
    float t0 = q / A;
    float t1 = C / q;

	/*float t0 = (-B + dSqrt) / (2.0f * A);
	float t1 = (-B - dSqrt) / (2.0f * A);*/

    // make sure t0 is smaller than t1
    if (t0 > t1)
    {
        // if t0 is bigger than t1 swap them around
        float temp = t0;
        t0 = t1;
        t1 = temp;
    }

    // if t1 is less than zero, the object is in the ray's negative direction
    // and consequently the ray misses the sphere
    if (t1 < 0){
        rec.anyHit =  false;
	}

	const float eps = pow(10.0f,-4.0f);

    // if t0 is less than zero, the intersection point is at t1
    if (abs(t0) < eps/*< 0*/)
    {
        rec.tHit = t1;
        rec.anyHit =  true;

		if(t1 < eps)//Too close look if the other is the awnser
		{
			if(abs(t0) > eps)rec.tHit = t0;
			else rec.anyHit =  false;
		}
    }
    // else the intersection point is at t0
    else
    {
        rec.tHit = t0;
        rec.anyHit =  true;

		if(t0 < eps)//Too close look if the other is the awnser
		{
			if(abs(t1) > eps)rec.tHit = t1;
			else rec.anyHit =  false;
		}
    }

	return rec.anyHit;
}

void Sphere::computeSurfaceHitFields(const Ray & r, HitRec & rec) const {
	
	

	rec.p = r.o + r.d * rec.tHit;
	rec.n = (rec.p - c).normalize();
	
	/*rec.p.r = color.r;
	rec.p.g = color.g;
	rec.p.b = color.b;*/

}


//bool Sphere::hit(const Ray & r, HitRec & rec, float & numberoftests) const {	
//
//	Vec3f v = c - r.o;
//	float s = v.dot(r.d);
//	float vLenSq = v.dot(v);
//	float radSq = this->r * this->r; 
//
//	rec.tHit = 0.0f;
//
//	if (s < 0 && vLenSq > radSq) {
//		rec.anyHit =  false;
//		return rec.anyHit;
//	}
//
//	float mSq = vLenSq - s * s;
//
//	if (mSq > radSq) {
//		rec.anyHit =  false;
//		return rec.anyHit;
//	}
//
//	++numberoftests;
//	Vec3f rc = r.o - c;
//
//	float A = r.d.dot(r.d);
//	float B = 2 * (rc).dot(r.d);
//	float C = (rc).dot(rc) - radSq;
//	float delta = B*B - 4 * A * C;
//
//	float dSqrt;
//	dSqrt = sqrtf(delta);
//
//	float q;
//    if (B < 0)
//        q = (-B - dSqrt)/2.0f;
//    else
//        q = (-B + dSqrt)/2.0f;
//
//    // compute t0 and t1
//    float t0 = q / A;
//    float t1 = C / q;
//
//    // make sure t0 is smaller than t1
//    if (t0 > t1)
//    {
//        // if t0 is bigger than t1 swap them around
//        float temp = t0;
//        t0 = t1;
//        t1 = temp;
//    }
//
//    // if t1 is less than zero, the object is in the ray's negative direction
//    // and consequently the ray misses the sphere
//    if (t1 < 0){
//        rec.anyHit =  false;
//	}
//
//	const float eps = pow(10.0f,-4.0f);
//
//    // if t0 is less than zero, the intersection point is at t1
//    if (abs(t0) < eps/*< 0*/)
//    {
//        rec.tHit = t1;
//        rec.anyHit =  true;
//
//		if(t1 < eps)//Too close look if the other is the awnser
//		{
//			if(abs(t0) > eps)rec.tHit = t0;
//			else rec.anyHit =  false;
//		}
//    }
//    // else the intersection point is at t0
//    else
//    {
//        rec.tHit = t0;
//        rec.anyHit =  true;
//
//		if(t0 < eps)//Too close look if the other is the awnser
//		{
//			if(abs(t1) > eps)rec.tHit = t1;
//			else rec.anyHit =  false;
//		}
//    }
//
//	//if(dSqrt < 0 )
//	//{
//	//	rec.tHit = 0.0f;
//	//	rec.anyHit = false;
//	//	return false;
//	//}
//	//
//	//t1 = (-B + dSqrt) / (2.0f/* * A*/);
//	//t2 = (-B - dSqrt) / (2.0f/* * A*/);
//
//	//if(dSqrt == 0)
//	//{
//	//	rec.tHit = t1;
//	//	rec.anyHit = true;
//	//}else
//	//{
//	//	if(t1 > 0 && t2 > 0)
//	//	{
//	//		if(t1 < t2)rec.tHit = t1;else rec.tHit = t2;
//	//		rec.anyHit = true;
//	//	}else
//	//	{
//	//		if(t1 > 0)rec.tHit = t1;else rec.tHit = t2;
//	//		rec.anyHit = true;
//	//	}
//	//}
//	return rec.anyHit;
//}