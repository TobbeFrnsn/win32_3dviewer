#ifndef _SCENE_H_
#define _SCENE_H_


#include<iostream>
#include <sstream>
#include <vector>

//#include "GL/glew.h"
//#include "GL/glut.h"

#include "algebra.h"
#include "utilities.h"
#include "mesh.h"

#include "Vec3.h"
#include "Image.h"
#include "Ray.h"
#include "Sphere.h"
#include "Camera.h"
#include "Light.h"
#include "bitmap.h"

#define NUM_OF_LIGHTS 1

class Scene {
public:

	Light lights[NUM_OF_LIGHTS];
	vector<Sphere> spheres;		
	Camera cam;

	Scene(void) {
		 
	}

	void add(Sphere & s) {
		spheres.push_back(s); 
		//cout << "Sphere added: " << "r = " << spheres[spheres.size()-1].r << endl;
	}

	float split(string &s2, string &s1, char delim){
		
		size_t i;
		s2 = "";
		i=s1.find_first_of(delim);
		s2.append(s1, 0, i);
		s1.erase(0, i + 1);

		float num;
		stringstream ss(s2.c_str());
		ss>>num;

		return num;
	}

	void load(const char * fileName) {
		// load a file with spheres for your scene here ...

		//FILE INPUT: {cen.x} {cen.y} {cen.z} {rad} {col.r} {col.g} {col.b} {refl} {ambiemt} {diffuse} {specular}

		ifstream myfile (fileName);

		//myfile.open (fileName);

		if (myfile.is_open())
		{
			string data = "";
			string line = "";

			//Initiation variables
			Vec3f cen; //Center
			float rad; //Radius
			Vec3f col; //Color
			float refl; //Reflection 

			//Setup material variables
			float a; //Amient
			float d; //Diffuse
			float s; //Specular

			while ( myfile.good() )
			{
				getline (myfile,line);
				//Center
				cen.x = split(data,line,' '); //cen.x/*::atof(data.c_str());*/
				cen.y = split(data,line,' '); //cen.y/*::atof(data.c_str());*/
				cen.z = split(data,line,' '); //cen.z/*::atof(data.c_str());*/
				//Radius
				rad = split(data,line,' '); //rad/*::atof(data.c_str());*/
				//Color
				col.r = split(data,line,' '); //col.r/*::atof(data.c_str());*/
				col.g = split(data,line,' '); //col.g/*::atof(data.c_str());*/
				col.b = split(data,line,' '); //col.b/*::atof(data.c_str());*/
				//Reflection
				refl = split(data,line,' '); //refl/*::atof(data.c_str());*/
				//Setup material variables
				a = split(data,line,' '); //ambient/*::atof(data.c_str());*/
				d = split(data,line,' '); //diffuse/*::atof(data.c_str());*/
				s = split(data,line,' '); //specular/*::atof(data.c_str());*/

				add(Sphere(cen,rad,col,refl));
				spheres[spheres.size() - 1].setupMaterial(a,d,s);

			}
			myfile.close();
		}
		else{

			cout << "Unable to open file"; 
		}
	}

};






#endif