#ifndef _BITMAP_H_
#define _BITMAP_H_

#include <sstream>
#include <fstream>
#include <windows.h>
#include <tchar.h>
#include<iostream>

#include "Image.h"

#define WIDTH 600
#define HEIGHT	600		

/* Used Sources
	
	xhttp://msdn.microsoft.com/en-us/library/bwea7by5(VS.80).aspx

	xhttp://msdn.microsoft.com/en-us/library/windows/desktop/dd145119(v=vs.85).aspx

*/

PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp);

bool CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, 
                  HBITMAP hBMP, HDC hDC);

inline int GetFilePointer(HANDLE FileHandle){
	return SetFilePointer(FileHandle, 0, 0, FILE_CURRENT);
};

bool SaveBMPFile(const wchar_t *filename, HBITMAP bitmap, HDC bitmapDC, int width, int height/*,BYTE * image,int image_size*/);

void writeBMP(Image * image,const wchar_t * filename);

#endif