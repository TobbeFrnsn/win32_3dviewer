//#include <stdlib.h>
//#include "mesh.h"
//
//void insertModel(Mesh **list, int nv, float * vArr, int nt, int * tArr, double scale, double posX, double posY, double posZ, double rotX, double rotY, double rotZ,double scaleXYZ) {
//	Mesh * mesh = (Mesh *) malloc(sizeof(Mesh));
//	mesh->nv = nv;
//	mesh->nt = nt;	
//	mesh->vertices = (Vector *) malloc(nv * sizeof(Vector));
//	mesh->vertexNormals = (Vector *) malloc(nv * sizeof(Vector));
//	mesh->triangles = (Triangle *) malloc(nt * sizeof(Triangle));
//	
//	for (int i = 0; i < nv; i++) {
//		mesh->vertices[i].x = vArr[i*3] * scale;
//		mesh->vertices[i].y = vArr[i*3+1] * scale;
//		mesh->vertices[i].z = vArr[i*3+2] * scale;
//		mesh->vertexNormals[i].x = 0; 
//		mesh->vertexNormals[i].y = 0; 
//		mesh->vertexNormals[i].z = 0; 
//	}
//	
//	for (int i = 0; i < nt; i++) {
//		mesh->triangles[i].vInds[0] = tArr[i*3];
//		mesh->triangles[i].vInds[1] = tArr[i*3+1];
//		mesh->triangles[i].vInds[2] = tArr[i*3+2];
//	}
//
//	mesh->rotation.x = 0;
//	mesh->rotation.y = 0;
//	mesh->rotation.z = 0;
//
//	mesh->scaling.x = 1;
//	mesh->scaling.y = 1;
//	mesh->scaling.z = 1;
//
//	mesh->translation.x = 0;
//	mesh->translation.y = 0;
//	mesh->translation.z = -20;
//
//	//Just set up the bounding sphere to default
//	mesh->boundingSphere.center.x = 0;
//	mesh->boundingSphere.center.y = 0;
//	mesh->boundingSphere.center.z = 0;
//	mesh->boundingSphere.radius = 0;
//	mesh->next = *list;
//	*list = mesh;	
//}
